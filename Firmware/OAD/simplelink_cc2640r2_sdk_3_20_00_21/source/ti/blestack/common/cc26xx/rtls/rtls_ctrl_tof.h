/******************************************************************************

 @file  rtls_ctrl_tof.h

 @brief This file contains the functions and structures specific to
        ToF post processing
 Group: WCS, BTS
 Target Device: cc2640r2

 ******************************************************************************
 
 Copyright (c) 2018-2019, Texas Instruments Incorporated
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:

 *  Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

 *  Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.

 *  Neither the name of Texas Instruments Incorporated nor the names of
    its contributors may be used to endorse or promote products derived
    from this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 ******************************************************************************
 
 
 *****************************************************************************/


/**
 *  @defgroup RTLS_CTRL RTLS_CTRL
 *  @brief This module implements Real Time Localization System (RTLS) Control module
 *
 *  @{
 *  @file  rtls_ctrl_tof.h
 *  @brief      This file contains the functions and structures specific to
 *              ToF post processing
 */

#ifndef RTLS_CTRL_TOF_H_
#define RTLS_CTRL_TOF_H_

#ifdef __cplusplus
extern "C"
{
#endif

/*********************************************************************
 * INCLUDES
 */

#include "TOF.h"

/*********************************************************************
*  EXTERNAL VARIABLES
*/

/*********************************************************************
 * CONSTANTS
 */

#define RTLS_CTRL_TOF_RSSI_THRESHOLD_HYSTERESIS -5     //!< RSSI Hysteresis threshold
#define MAX_TOF_SAMPLES_SINGLE_CHUNK 16                //!< Max Num of Samples

/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * TYPEDEFS
 */


/** @defgroup RTLS_CTRL_Structs RTLS Control Structures
 * @{
 */

/// @brief Enumeration for ToF Result Mode
typedef enum
{
  TOF_MODE_DIST,
  TOF_MODE_STAT,
  TOF_MODE_RAW
} tofResultMode_e;

/// @brief Enumeration for ToF Run Mode
typedef enum
{
  TOF_MODE_CONT,
  TOF_MODE_ONE_SHOT,
  TOF_MODE_AUTO
} tofRunMode_e;

/// @brief Enumeration for ToF calibration states
typedef enum
{
  TOF_CALIB_NOT_INITIALIZED,
  TOF_CALIB_CALIBRATING,
  TOF_CALIB_CALIBRATED
} tofCalibState_e;

/// @brief  Various ToF configurations
typedef struct
{
  int8_t          autoTofRssiTresh;   //!< RSSI threshold
  tofResultMode_e resultMode;         //!< On/off-chip result processing
  tofRunMode_e    runMode;            //!< One-shot/continuous/auto
  uint8_t         constSyncwords;     //!< Syncwords are generated only once
} rtlsTofConfig_t;

/// @brief  ToF calibration information
typedef struct
{
  tofCalibState_e tofCalibState;  //!< Calibration state
  uint8_t runsForCalibration;     //!< Amount runs (instances of TOF_run) needed until calibration is complete
  uint16_t calibrationOffset;     //!< The distance at which calibration is being performed (will be subtracted from final result)
  ToF_BurstStat *pCalibVals;      //!< Array to save calibration values
} rtlsTofCalib_t;

/// @brief  ToF Parameters - Received from RTLS Node Manager and passed onto RTLS Slave via formed connection
typedef struct __attribute__((packed))
{
  ToF_Role        tofRole;            //!< ToF Role
  uint16_t        numBurstSamples;    //!< Number of Sync Words for each ToF run
  uint8_t         numFreq;            //!< Number of frequencies
  rtlsTofConfig_t tofConfig;          //!< ToF Configuration
  uint16_t        frequencies[];      //!< Frequencies
} rtlsTofParams_t;

/// @brief This will be used to distribute the ToF Security initial seed to the RTLS Passive nodes
typedef struct __attribute__((packed))
{
  uint8_t seed[TOF_SEEDLEN];          //!< ToF Security seed
} rtlsTofSecSeed_t;

/// @brief ToF Enable command
typedef struct __attribute__((packed))
{
  uint8_t enableTof;    //!< Enable or disable ToF
} rtlsEnableTofCmd_t;

/// @brief ToF Calibration
typedef struct __attribute__((packed))
{
  uint8_t enableCalib;      //!< Enable/disable calibration
  uint16_t samplesPerFreq;  //!< Amount of samples required per frequency until calibration is complete
  uint8_t calibDistance;    //!< The distance at which calibration is being performed (will be added to the final result)
} rtlsEnableTofCalib_t;

/// @brief ToF Result - On-chip calculation mode
typedef struct __attribute__((packed))
{
  double distance;              //!< Distance in meters
  int8_t rssi;                  //!< Average rssi
} rtlsTofResultDistance_t;

/// @brief ToF Result - Raw
typedef struct __attribute__((packed))
{
  uint32_t tick;                //!< Time
  uint8_t freqIdx;              //!< Index of frequency in configured frequency array
  int8_t rssi;                  //!< Rssi of this measurement
} rtlsTofResultRaw_t;

/// @brief ToF Result - Raw Aggragation
typedef struct __attribute__((packed))
{
  uint16_t offset;              //!< ToF Result - Raw Aggragation of offset
  uint16_t samplesLength;       //!< ToF Result - Sample length
  rtlsTofResultRaw_t samples[]; //!< ToF Result - Raw Aggragation samples
} rtlsTofResultRawAggr_t;

/// @brief ToF Result - Statistics
typedef struct __attribute__((packed))
{
  uint16_t freq;                //!< Statistics for this frequency
  double tick;                  //!< Time
  double tickVariance;          //!< Variance between measurements
  int8_t rssi;                  //!< Average rssi between measurements
  uint32_t numOk;               //!< Number of valid measurements
} rtlsTofResultStat_t;

/// @brief Aggregation of ToF structures needed for RTLS Control operation (saved in gRtlsData)
typedef struct
{
  ToF_Struct tofStruct;           //!< Struct will be filled by TOF_open
  ToF_Handle tofHandle;           //!< Handle will be passed to the TOF driver for actions
  rtlsTofConfig_t tofConfig;      //!< Values configured by the user
  rtlsTofCalib_t tofCalibInfo;    //!< Information about ToF calibration
  ToF_BurstStat *pTofLastRun;     //!< Array to save samples from the last ToF run
  ToF_BurstStat *pTofAverage;     //!< Array to save average of samples (for each frequency)
  uint8_t bTofValuesInitialized;  //!< Flag to check if ToF was run at least once
  uint8_t bOneShotSuccessful;     //!< Used to determine whether a one shot ToF run was successful
  uint8_t bSlaveTofParamPend;     //!< Used to determine whether to send params to slave when a connection is formed
  uint8_t bSlaveTofEnabled;       //!< Used to determine whether slave is doing ToF or not
} rtlsTof_t;
/** @} End RTLS_CTRL_Structs */

/*********************************************************************
 * API FUNCTIONS
 */

/**
* @brief   Post process a ToF run
*
* @param   tofControlBlock - pointer to ToF information
* @param   rssi - current rssi
*
* @return  None
*/
void RTLSCtrl_postProcessTof(rtlsTof_t *tofControlBlock, int8_t rssi);

/**
* @brief   Output array of ToF_BurstStat
*
* @param   cmdId - RTLS_CMD_TOF_* - command to the host
* @param   numFreq - needed for size calculation
* @param   tofStatArray - Array to output
* @param   rssi - current rssi
*
* @return  None
*/
void RTLSCtrl_outputTofStat(uint8_t cmdId, uint16_t numFreq, ToF_BurstStat *tofStatArray, uint8_t rssi);

/*********************************************************************
*********************************************************************/

#ifdef __cplusplus
}
#endif

#endif /* RTLS_CTRL_TOF_H_ */

/** @} End RTLS_CTRL */
