/******************************************************************************

 @file  rtls_ctrl_tof.c

 @brief This file is used to do ToF post processing

 Group: WCS, BTS
 Target Device: cc2640r2

 ******************************************************************************
 
 Copyright (c) 2018-2019, Texas Instruments Incorporated
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:

 *  Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

 *  Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.

 *  Neither the name of Texas Instruments Incorporated nor the names of
    its contributors may be used to endorse or promote products derived
    from this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 ******************************************************************************
 
 
 *****************************************************************************/

/*********************************************************************
 * INCLUDES
 */

#include "rtls_ctrl_tof.h"
#include "rtls_ctrl_api.h"
#include "rtls_ctrl.h"
#include "rtls_host.h"

#include <ti/sysbios/hal/Hwi.h>
#include <ti/sysbios/knl/Swi.h>
#include <stdlib.h>

/*********************************************************************
 * MACROS
 */

#define IIR_AVG(attack, decay, old, new) (new > old ? (attack * (float)new + (1-attack) * (float)old) : (decay * (float)new + (1-decay) * (float)old))
#define CALC_PERCENTAGE(percent, b) (uint16_t)(percent*b)/100;



/*********************************************************************
 * CONSTANTS
 */

// IIR filter values
#define TOF_FILTER_ATTACK                     0.7
#define TOF_FILTER_DECAY                      0.7

/*********************************************************************
 * TYPEDEFS
 */


/*********************************************************************
 * GLOBAL VARIABLES
 */

/*********************************************************************
 * EXTERNAL VARIABLES
 */

/*********************************************************************
 * EXTERNAL FUNCTIONS
 */

/*********************************************************************
 * LOCAL VARIABLES
 */

/*********************************************************************
 * LOCAL FUNCTIONS
 */

ToF_BurstStat RTLSCtrl_tofGetTotalAvg(rtlsTof_t *tofControlBlock);
void RTLSCtrl_calcMovingAverage(rtlsTof_t *tofControlBlock);
uint8_t RTLSCtrl_validateTofRun(rtlsTof_t *tofControlBlock);

/*********************************************************************
 * PUBLIC FUNCTIONS
 */

/*********************************************************************
* @fn      RTLSCtrl_postProcessTof
*
* @brief   Post process a ToF run
*
* @param   tofControlBlock - pointer to ToF information
* @param   rssi - rssi of the last connection event
*
* @return  None
*/
void RTLSCtrl_postProcessTof(rtlsTof_t *tofControlBlock, int8_t rssi)
{
  ToF_BurstStat tofDrvResult;
  uint8_t tofRunValid;

  // Validate ToF run
  tofRunValid = RTLSCtrl_validateTofRun(tofControlBlock);
  
  if (tofRunValid == RTLS_FAIL)
  {
    return;
  }

  // If we are configured to perform one-shot ToF and we had enough samples (i.e function did not return yet)
  // Mark that at least one ToF run was successful at this point
  if (tofControlBlock->tofConfig.runMode == TOF_MODE_ONE_SHOT &&
     ((tofControlBlock->tofCalibInfo.tofCalibState == TOF_CALIB_CALIBRATED) || tofControlBlock->tofConfig.resultMode != TOF_MODE_DIST))
  {
    tofControlBlock->bOneShotSuccessful = RTLS_TRUE;
  }

  // Calculate results with respect to resultMode
  switch (tofControlBlock->tofConfig.resultMode)
  {
    case TOF_MODE_DIST:
    {
      rtlsTofResultDistance_t distanceRes;

      RTLSCtrl_calcMovingAverage(tofControlBlock);
      tofDrvResult = RTLSCtrl_tofGetTotalAvg(tofControlBlock);

      // Countdown until ToF is calibrated (calibration is done only in TOF_MODE_DIST)
      if (tofControlBlock->tofCalibInfo.tofCalibState == TOF_CALIB_CALIBRATING)
      {
        // This condition handles the case where we are asked to perform infinite calibration
        // if we meet this condition, calibration will endlessly continue
        if (tofControlBlock->tofCalibInfo.runsForCalibration == 0)
        {
          break;
        }

        // Countdown for calibration
        // Note that subtracting should be done before testing if we are finished
        // This enables the user to perform infinite calibration, stopping it with a host command
        tofControlBlock->tofCalibInfo.runsForCalibration--;

        // Once we are finished, notify the host
        if (tofControlBlock->tofCalibInfo.runsForCalibration <= 0)
        {
          // Output calibration values when we are done calibrating
          RTLSCtrl_outputTofStat(RTLS_CMD_TOF_CALIBRATE,
                                 tofControlBlock->tofHandle->numFreqs,
                                 tofControlBlock->tofCalibInfo.pCalibVals,
                                 rssi);

          // Mark that calibration array is initialized and can be used for offsetting
          tofControlBlock->tofCalibInfo.tofCalibState = TOF_CALIB_CALIBRATED;
        }

        // No results until we are calibrated
        break;
      }

      // Calculate the final distance by converting ticks to meters and subtracting the distance from which calibration was performed
      distanceRes.distance = (tofDrvResult.tick * TICK_TO_METER) + tofControlBlock->tofCalibInfo.calibrationOffset;
      distanceRes.rssi = rssi;

      RTLSHost_sendMsg(RTLS_CMD_TOF_RESULT_DIST, HOST_ASYNC_RSP, (uint8_t *)&distanceRes, sizeof(rtlsTofResultDistance_t));
    }
    break;

    case TOF_MODE_STAT:
    {
      RTLSCtrl_outputTofStat(RTLS_CMD_TOF_RESULT_STAT,
                             tofControlBlock->tofHandle->numFreqs,
                             tofControlBlock->pTofLastRun,
                             rssi);
    }
    break;

    case TOF_MODE_RAW:
    {
      rtlsTofResultRawAggr_t *tofResultAggr;
      uint16_t numTofSamples;
      uint16_t samplesToOutput;
      ToF_Sample *pIter;
      rtlsTofResultRaw_t *pSamples;
      int i;

      tofResultAggr = RTLSCtrl_malloc(sizeof(rtlsTofResultRawAggr_t) + (MAX_TOF_SAMPLES_SINGLE_CHUNK * sizeof(rtlsTofResultRaw_t)));
      pIter = tofControlBlock->tofHandle->pT1RSSIBuf; 

      // If the array is not empty
      if (pIter != NULL && tofResultAggr != NULL)
      {
        // Calculate how many samples we will be outputting
        numTofSamples = tofControlBlock->tofHandle->numBurstSamples / 2; 

        tofResultAggr->samplesLength = numTofSamples;
        tofResultAggr->offset = 0;

        do
        {
          // If the remainder is larger than buff size, tx maximum buff size
          if (tofResultAggr->samplesLength - tofResultAggr->offset > MAX_TOF_SAMPLES_SINGLE_CHUNK)
          {
            samplesToOutput = MAX_TOF_SAMPLES_SINGLE_CHUNK;
          }
          else
          {
            // If not, then output the remaining data
            samplesToOutput = tofResultAggr->samplesLength - tofResultAggr->offset;
          }

          // Copy the samples to output buffer
          pSamples = tofResultAggr->samples;
          for (i = 0; i < samplesToOutput; i++)
          {
            pSamples[i].freqIdx = pIter[i].freqIndex;
            pSamples[i].tick = pIter[i].T1;
            pSamples[i].rssi = pIter[i].RSSI;
          }

          RTLSHost_sendMsg(RTLS_CMD_TOF_RESULT_RAW, HOST_ASYNC_RSP, (uint8_t *)tofResultAggr, sizeof(rtlsTofResultRawAggr_t) + (sizeof(rtlsTofResultRaw_t) * samplesToOutput));

          tofResultAggr->offset += samplesToOutput;
          pIter += samplesToOutput;
        }
        while (tofResultAggr->offset < tofResultAggr->samplesLength);

     }
 
      if (tofResultAggr)
      {
        RTLSUTIL_FREE(tofResultAggr);
      }
    }
    break;
    
    default:
      break;
  }

  TOF_clearBuffers(tofControlBlock->tofHandle);
  memset(tofControlBlock->pTofLastRun, 0, sizeof(ToF_BurstStat) * tofControlBlock->tofHandle->numFreqs);
}

/*********************************************************************
* @fn      RTLSCtrl_tofGetTotalAvg
*
* @brief   Calculate total average
*
* @param   tofControlBlock - pointer to ToF information
*
* @return  None
*/
ToF_BurstStat RTLSCtrl_tofGetTotalAvg(rtlsTof_t *tofControlBlock)
{
  ToF_BurstStat res = {0};

  for (int i = 0; i < tofControlBlock->tofHandle->numFreqs; ++i)
  {
    res.numOk += tofControlBlock->pTofAverage[i].numOk;
    
    res.tick += tofControlBlock->pTofAverage[i].tick;
    
    if (tofControlBlock->tofCalibInfo.tofCalibState == TOF_CALIB_CALIBRATED)
    {
      res.tick -= tofControlBlock->tofCalibInfo.pCalibVals[i].tick;
    }
    
    res.tickVariance += tofControlBlock->pTofAverage[i].tickVariance;
  }

  res.numOk        /= tofControlBlock->tofHandle->numFreqs;
  res.tick         /= tofControlBlock->tofHandle->numFreqs;
  res.tickVariance /= tofControlBlock->tofHandle->numFreqs;

  return res;
}

/*********************************************************************
* @fn      RTLSCtrl_calcMovingAverage
*
* @brief   Calculate moving average for each frequency
*
* @param   tofControlBlock - pointer to ToF information
*
* @return  None
*/
void RTLSCtrl_calcMovingAverage(rtlsTof_t *tofControlBlock)
{
  if (!tofControlBlock->bTofValuesInitialized)
  {
    for (int i = 0; i < tofControlBlock->tofHandle->numFreqs; i++)
    {
      tofControlBlock->pTofAverage[i] = tofControlBlock->pTofLastRun[i];
    }
    tofControlBlock->bTofValuesInitialized = RTLS_TRUE;
  }

  if ((tofControlBlock->tofConfig.resultMode == TOF_MODE_DIST) &&
      (tofControlBlock->tofCalibInfo.tofCalibState == TOF_CALIB_CALIBRATING))
  {
    for (int i = 0; i < tofControlBlock->tofHandle->numFreqs; i++)
    {
      tofControlBlock->tofCalibInfo.pCalibVals[i] = tofControlBlock->pTofAverage[i];
    }
  }

  for (int i = 0; i < tofControlBlock->tofHandle->numFreqs; ++i)
  {
    tofControlBlock->pTofAverage[i].freq         =  tofControlBlock->pTofLastRun[i].freq;
    tofControlBlock->pTofAverage[i].numOk        =  tofControlBlock->pTofLastRun[i].numOk;
    tofControlBlock->pTofAverage[i].tick         =  IIR_AVG(TOF_FILTER_ATTACK, TOF_FILTER_DECAY, tofControlBlock->pTofAverage[i].tick, tofControlBlock->pTofLastRun[i].tick);
    tofControlBlock->pTofAverage[i].tickVariance =  IIR_AVG(TOF_FILTER_ATTACK, TOF_FILTER_DECAY, tofControlBlock->pTofAverage[i].tickVariance, tofControlBlock->pTofLastRun[i].tickVariance);
  }
}

/*********************************************************************
* @fn      RTLSCtrl_outputTofStat
*
* @brief   Output array of ToF_BurstStat
*
* @param   cmdId - RTLS_CMD_TOF_* - command to the host
* @param   numFreq - needed for size calculation
* @param   tofStatArray - Array to output
* @param   rssi - current rssi
*
* @return  None
*/
void RTLSCtrl_outputTofStat(uint8_t cmdId, uint16_t numFreq, ToF_BurstStat *tofStatArray, uint8_t rssi)
{
  rtlsTofResultStat_t *tofResultStat;
  uint16_t statSize;

  statSize = sizeof(rtlsTofResultStat_t) *numFreq;

  if ((tofResultStat = (RTLSCtrl_malloc(statSize))) == NULL)
  {
    // We failed to allocate, host was already notified, just exit
    return;
  }

  // Copy to a packed structure
  for (int i = 0; i < numFreq; i++)
  {
    tofResultStat[i].freq = tofStatArray[i].freq;
    tofResultStat[i].numOk = tofStatArray[i].numOk;
    tofResultStat[i].tick = tofStatArray[i].tick;
    tofResultStat[i].tickVariance = tofStatArray[i].tickVariance;
    tofResultStat[i].rssi = rssi;
  }

  RTLSHost_sendMsg(cmdId, HOST_ASYNC_RSP, (uint8_t *)tofResultStat, statSize);

  // Free the result array
  RTLSUTIL_FREE(tofResultStat);
}

/*********************************************************************
* @fn      RTLSCtrl_validateTofRun
*
* @brief   Validate the ToF run
*
* @param   tofControlBlock - pointer to ToF information
*
* @return  success/fail
*/
uint8_t RTLSCtrl_validateTofRun(rtlsTof_t *tofControlBlock)
{
  uint8_t minNumValidSamples;
  uint8_t resPerFreq = tofControlBlock->tofHandle->numBurstSamples/tofControlBlock->tofHandle->numFreqs;
  uint8_t ret = RTLS_SUCCESS;

  // At lest 10% of the samples should be good ones
  minNumValidSamples = CALC_PERCENTAGE(10, resPerFreq);

  // 0 is not allowed
  if (minNumValidSamples == 0)
  {
    minNumValidSamples = 1;
  }

  TOF_getBurstStat(tofControlBlock->tofHandle, &tofControlBlock->pTofLastRun);

  for (int i = 0; i < tofControlBlock->tofHandle->numFreqs; ++i)
  {
    // If we do not have enough samples, drop the run
    if (tofControlBlock->pTofLastRun[i].numOk < minNumValidSamples)
    {
      TOF_clearBuffers(tofControlBlock->tofHandle);
      memset(tofControlBlock->pTofLastRun, 0, sizeof(ToF_BurstStat) * tofControlBlock->tofHandle->numFreqs);

      ret = RTLS_FAIL;

      return ret;
    }
  }
  return ret;
}
