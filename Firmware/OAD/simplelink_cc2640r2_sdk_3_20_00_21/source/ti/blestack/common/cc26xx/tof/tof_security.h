/******************************************************************************

 @file  tof_security.h

 @brief This file contains methods to enable/disable and control TOF
 Group: WCS, BTS
 Target Device: cc2640r2

 ******************************************************************************
 
 Copyright (c) 2018-2019, Texas Instruments Incorporated
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:

 *  Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

 *  Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.

 *  Neither the name of Texas Instruments Incorporated nor the names of
    its contributors may be used to endorse or promote products derived
    from this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 ******************************************************************************
 
 
 *****************************************************************************/

/**
 *  @defgroup TOF TOF
 *  @brief This module implements the Time of Flight (TOF)
 *
 *  @{
 *  @file  tof_security.h
 *  @brief      ToF security module interface
 */

#ifndef TOF_SECURITY_H_
#define TOF_SECURITY_H_

#ifdef __cplusplus
extern "C"
{
#endif

/*********************************************************************
 * INCLUDES
 */

#include <stdint.h>
#include <ti/drivers/crypto/CryptoCC26XX.h>

/*********************************************************************
 * MACROS
 */
/// @brief NUM_ELEMENTS calculates the num of elements in an array
#define NUM_ELEMENTS(x)                 (sizeof(x)/sizeof(x[0]))

/*********************************************************************
 * CONSTANTS
 */

// CTR-DRBG definitions
#define CTR_DRBG_BLOCKLEN               16                                             //!< Length of CTR DRBG Block
#define CTR_DRBG_SEEDLEN                (CTR_DRBG_BLOCKLEN + CTR_DRBG_BLOCKLEN)        //!< Length of CTR DRBG Seed
#define CTR_DRBG_INST_FLAG              0xAA                                           //!< CTR DRBG Flag

#define TOF_SEC_FILL_BOTH_BUFFS         0x01                                           //!< Fill both buffers
#define TOF_SEC_FILL_1ST_BUFF           0x02                                           //!< Fill first buffers
#define TOF_SEC_FILL_2ND_BUFF           0x03                                           //!< Fill second buffers

#define TOF_SEC_SYNCWORD_SIZE           8                                              //!< Length of Syncword

#define TOF_SEC_DBL_BUFF_SIZE           16                                             //!< Size of ToF_securoty double buffer

/*********************************************************************
 * TYPEDEFS
 */

/** @defgroup TOF_SEC_Structs TOF Security Structures
 * @{
 */

/// @brief ToF Security Mode
typedef enum
{
  TOF_MODE_SINGLE_BUF = 0,     //!< Single Buffer
  TOF_MODE_DBL_BUF             //!< Double Buffer
} tofSecMode_e;

/// @brief ToF Security Error Result
typedef enum 
{
  TOF_SEC_SUCCESS_E = 0,       //!< Success
  TOF_SEC_GEN_FAIL_E,          //!< Seed Generation fail
  TOF_SEC_AES_FAIL_E,          //!< AES fail
  TOF_SEC_ALLOC_FAIL_E,        //!< Allocation fail
  TOF_SEC_INVALID_PRMS_FAIL_E  //!< Invalid parameters fail
} tofSecErrRes_t;

/// @brief ctr DRBG State
typedef struct 
{
  uint8_t             key[CTR_DRBG_BLOCKLEN];   //!< Variable used for ctr DRBG State
  uint8_t             v[CTR_DRBG_BLOCKLEN];     //!< Variable used for ctr DRBG State
  uint8_t             instantiatedFlag;         //!< Variable used for ctr DRBG State
} ctrDrbgState_t;

/// @brief ToF Security Configuration Parameters
typedef struct
{
  uint8_t             bUseDoubleBuffer;           //!< Defaults to false
  uint16_t            syncWordSize;               //!< In bytes. Defaults to 8
  uint16_t            totalNumOfSyncWords;        //!< Defaults to 64
} tofSecCfgPrms_t;

/// @brief ToF Security Handle
typedef struct
{
  // Counting Deterministic Random Bit Generator
  ctrDrbgState_t      ctrDrbgState;                //!< ctr DRBG State
  
  tofSecCfgPrms_t     tofSecCfgParams;             //!< ToF Security Configuration Parameters

  // Sync Words
  uint16_t            numOfSyncWordsPerBuffer;     //!< Length of double buffer, both buffers must be equal length. The number of bursts in double buffer mode is still numBurstSamples
  uint8_t             *pSyncWordBuffer1;           //!< Pointer to the sync word buffer, used in both single and double buffer mode
  uint8_t             *pSyncWordBuffer2;           //!< Pointer to the second sync word buffer, used in double buffer mode only

  //Seed
  uint8_t             seed[CTR_DRBG_SEEDLEN];      //!< ctr DRBG Seed

  // Crypto
  CryptoCC26XX_Params encParams;                   //!< AES Encryption parameters
  CryptoCC26XX_Handle encHandle;                   //!< AES Encryption handle
  int                 encKey;                      //!< AES Encryption key
} tofSecHandle_t;

/** @} End TOF_SEC_Structs */

/*********************************************************************
 * PUBLIC FUNCTIONS
 */

/**
 * @brief       This function is used to open the tof security module
 *              (to default or to presets defined in tofSecCfgPrms) and
 *              reset the AES engine.
 *
 *              Note: This routine leaves CM3 Crypto interrupts disabled.
 *
 *              Note: This function allocates the key without specifying one.
 *                    The key is loaded as needed without releases the key
 *                    store.
 *
 * input parameters
 *
 * @param       tofSecCfgPrms  - A pointer to the user-defined tof security preset values.
 *
 * @param       tofSecHandle  - A pointer to an empty tof security handle.
 *
 * output parameters
 *
 * @param       tofSecHandle  - A pointer to the tof security state.
 *
 * @return      uint8_t - NULL if failed, other if succeeded
 */
int TOFSecurity_open(tofSecHandle_t *tofSecHandle, tofSecCfgPrms_t *tofSecCfgPrms);

/**
 * @brief       This function is used to close ToF Security and free buffers
 *
 * input parameters
 *
 * @param       tofSecHandle  - A pointer to an empty tof security handle.
 *
 * output parameters
 *
 * @return      uint8_t - NULL if failed, other if succeeded
 */
int TOFSecurity_close(tofSecHandle_t *tofSecHandle);

/**
* @brief   use TRNG HW module to generate a true 128-bit random seed
*
* input parameters
*
* @param   tofSecHandle  - A pointer to the tof security state.
*
* output parameters
*
* @param   tofSecHandle  - A pointer to the tof security state.
*
* @return  NULL if failed, other if succeeded
*/
int TOFSecurity_genSeed(tofSecHandle_t *tofSecHandle);

/**
* @brief    Will fill each Sync Word Buffer with numOfSyncWordsPerBuffer Sync Words
*
* input parameters
*
* @param   bufferToFill  - An enum describing which buffers to fill
*
* @param   tofSecHandle  - A pointer to the tof security state.
*
* output parameters
*
* @param   tofSecHandle  - A pointer to the tof security state.
*
* @return  NULL if failed, other if succeeded
*/
int TOFSecurity_genSyncWords(tofSecHandle_t *tofSecHandle, uint8_t bufferToFill);

/**
 * @brief       This function is used to reseed the ctr drbg
 *
 * input parameters
 *
 * @param       tofSecHandle  - A pointer to the tof security state.
 *
 * @param       newSeed  - A pointer to the 128-bit new seed with which to reinstantiate.
 *
 * output parameters
 *
 * @param       tofSecHandle  - A pointer to the tof security state.
 *
 * @return      int - NULL if failed, other if succeeded
 */
int TOFSecurity_setSeed(tofSecHandle_t *tofSecHandle, uint8_t *newSeed);

/*********************************************************************
*********************************************************************/

#ifdef __cplusplus
}
#endif

#endif // TOF_SECURITY_H_

/** @} End TOF */
