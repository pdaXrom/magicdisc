/*
 * Copyright (c) 2016-2019, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __BOARD_H
#define __BOARD_H

#define Board_MAGICDISC

#ifdef __cplusplus
extern "C" {
#endif

#include <ti/drivers/Board.h>

#define Board_initGeneral()     Board_init()  /* deprecated */

#include "MAGICDISC.h"

#define Board_shutDownExtFlash() MAGICDISC_shutDownExtFlash()
#define Board_wakeUpExtFlash() MAGICDISC_wakeUpExtFlash()

/* These #defines allow us to reuse TI-RTOS across other device families */

#define Board_SPI0              MAGICDISC_SPI0
#define Board_SPI_FLASH_CS      MAGICDISC_SPI_FLASH_CS
#define Board_FLASH_CS_ON       0
#define Board_FLASH_CS_OFF      1

#define Board_SPI_MASTER        MAGICDISC_SPI0
#define Board_SPI_SLAVE         MAGICDISC_SPI0
#define Board_SPI_MASTER_READY  MAGICDISC_SPI_MASTER_READY
#define Board_SPI_SLAVE_READY   MAGICDISC_SPI_SLAVE_READY

#define Board_RGB_DIN           MAGICDISC_GPIO_RGB_DIN
#define Board_RGB_SCLK          MAGICDISC_GPIO_RGB_SCLK
#define Board_RGB_LATCH         MAGICDISC_GPIO_RGB_LATCH
#define Board_RGB_NOE           MAGICDISC_GPIO_RGB_NOE
#define Board_RGB_NRESET        MAGICDISC_GPIO_RGB_NRESET

#define Board_PWM0              MAGICDISC_PWM0

//#define Board_I2C0              MAGICDISC_I2C0
//#define Board_I2C_ACCEL         Board_I2C0

#define Board_UART0             MAGICDISC_UART0

#define Board_WATCHDOG0         MAGICDISC_WATCHDOG0

#ifdef __cplusplus
}
#endif

#endif /* __BOARD_H */
