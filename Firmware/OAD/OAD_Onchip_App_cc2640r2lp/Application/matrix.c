#include <inttypes.h>
#include <math.h>
#include "matrix.h"

Dot3D *rotateX(Dot3D *dot, float angle)
{
    float cosa = cos(angle);
    float sina = sin(angle);
    float tempy = dot->y;
    dot->y = cosa * dot->y - sina * dot->z;
    dot->z = cosa * dot->z + sina * tempy;
    return dot;
}

Dot3D *rotateY(Dot3D *dot, float angle)
{
    float cosa = cos(angle);
    float sina = sin(angle);
    float tempz = dot->z;
    dot->z = cosa * dot->z - sina * dot->x;
    dot->x = cosa * dot->x + sina * tempz;
    return dot;
}

Dot3D *rotateZ(Dot3D *dot, float angle)
{
    float cosa = cos(angle);
    float sina = sin(angle);
    float tempx = dot->x;
    dot->x = cosa * dot->x - sina * dot->y;
    dot->y = cosa * dot->y + sina * tempx;
    return dot;
}

void matrixRotate(uint8_t src[8][8], uint8_t dst[8][16], float angleX, float angleY,
                  float angleZ)
{
    int x, y;
    int w2, h2;

    int w = 8;
    int h = 8;

    w2 = w / 2;
    h2 = h / 2;

    for (y = 0; y < h; y++) {
        for (x = 0; x < w; x++){
            Dot3D dot = { .x = x - w2, .y = y - h2, 0 };
            if (angleX) {
                rotateX(&dot, angleX);
            }
            if (angleY) {
                rotateY(&dot, angleY);
            }
            if (angleZ) {
                rotateZ(&dot, angleZ);
            }
            dot.x += w2;
            dot.y += h2;

            int dx = roundf(dot.x);// - 1;
            int dy = roundf(dot.y);

            if (dx >= 0 && dx <= (w - 1) && dy >= 0 && dy <= (h - 1)) {
                dst[dy][dx] = src[y][x];
            }

        }
    }
}
