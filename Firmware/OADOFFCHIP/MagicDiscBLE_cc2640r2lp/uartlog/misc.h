/*
 * misc.h
 *
 *  Created on: 3 ����. 2019 �.
 *      Author: sash
 */

#ifndef UARTLOG_MISC_H_
#define UARTLOG_MISC_H_

char *utoa(unsigned int value, char *str, int base);

char *itoa(int value, char *str, int base);

#endif /* UARTLOG_MISC_H_ */
