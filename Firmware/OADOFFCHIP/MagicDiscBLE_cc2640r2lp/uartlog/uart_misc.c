#include <stdint.h>
#include <stddef.h>
#include <string.h>

#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/knl/Task.h>
#include <xdc/runtime/Error.h>

/* Driver Header files */
//#include <ti/drivers/GPIO.h>
//#include <ti/drivers/PWM.h>
//#include <ti/drivers/I2C.h>
#include <ti/drivers/UART.h>

//#include "simple_printf.h"
#include "misc.h"

static UART_Handle _uart;

void UART_setup(void *uart)
{
    _uart = uart;
}

void UART_writeChar(char c)
{
    UART_write(_uart, &c, 1);
}

void UART_writeString(char *string)
{
    UART_write(_uart, string, strlen(string));
}

void UART_writelnString(char *string)
{
    UART_write(_uart, string, strlen(string));
    UART_writeString("\r\n");
}

void UART_writeInt(int i)
{
    char buf[64];
    itoa(i, buf, 10);
    UART_writeString(buf);
}

void UART_writeUInt(int i)
{
    char buf[64];
    utoa(i, buf, 10);
    UART_writeString(buf);
}

void UART_writeHex(unsigned int h)
{
    char buf[64];
    utoa(h, buf, 16);
    UART_writeString(buf);
}

void UART_writeNl()
{
    UART_writeString("\r\n");
}
