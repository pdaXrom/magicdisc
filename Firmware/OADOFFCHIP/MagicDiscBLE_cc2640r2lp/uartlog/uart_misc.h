/*
 * uart_misc.h
 *
 *  Created on: 12 ����. 2019 �.
 *      Author: sash
 */

#ifndef UART_MISC_H_
#define UART_MISC_H_

void UART_setup(void *uart);

void UART_writeChar(char c);

void UART_writeString(char *string);

void UART_writelnString(char *string);

void UART_writeInt(int i);

void UART_writeUInt(int i);

void UART_writeHex(unsigned int h);

void UART_writeNl();

#endif /* UART_MISC_H_ */
