#include <stdlib.h>
#include <stdarg.h>

#include "simple_printf.h"
#include "syslog.h"

void openlog(char *ident, int option, int facility)
{
}

void closelog()
{
}

void syslog(int pri, const char *fmt, ...)
{
    va_list ap;

    // Print level, file and line
    char *infoStr = NULL;
    switch(pri) {
    case LOG_ERR:
        infoStr = "\x1b[31;1mERROR: \x1b[0m";
        break;
    case LOG_DEBUG:
        infoStr = "\x1b[33;1mDEBUG: \x1b[0m";
        break;
    case LOG_WARNING:
        infoStr = "\x1b[34;1mWARNING: \x1b[0m";
        break;
    case LOG_INFO:
    default:
        infoStr = "\x1b[32;1mINFO: \x1b[0m";
    }

    simple_printf(infoStr);

    va_start(ap, fmt);
    simple_vsprintf(NULL, (char *)fmt, ap);
    va_end(ap);

    simple_printf("\r\n");
}
