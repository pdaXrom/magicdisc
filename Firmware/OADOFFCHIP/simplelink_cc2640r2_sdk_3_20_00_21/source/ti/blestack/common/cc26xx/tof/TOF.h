/******************************************************************************

 @file  TOF.h

 @brief This file contains methods to enable/disable and control TOF
 Group: WCS, BTS
 Target Device: cc2640r2

 ******************************************************************************
 
 Copyright (c) 2018-2019, Texas Instruments Incorporated
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:

 *  Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

 *  Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.

 *  Neither the name of Texas Instruments Incorporated nor the names of
    its contributors may be used to endorse or promote products derived
    from this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 ******************************************************************************
 
 
 *****************************************************************************/

/**
 *  @defgroup TOF TOF
 *  @brief This module implements the Time of Flight (TOF)
*
 *  @{
 *  @file  TOF.h
 *  @brief      TOF interface
 */


#ifndef TOF_H_
#define TOF_H_

#ifdef __cplusplus
extern "C"
{
#endif

/*********************************************************************
 * INCLUDES
 */

/* RF includes */
#include <ti/drivers/rf/RF.h>

#include <ti/devices/DeviceFamily.h>
#include "tof/rfc_cmd_tof.h"
#include "tof/tof_security.h"

/*********************************************************************
 * CONSTANTS
 */

#define TOF_TIMEOUTVAL       0xFFFFFFFF	        //!< TOF Timeout

#define TOF_MAX_NUM_FREQ        20	            //!< Maximun number of frequencies

#define TOF_SEEDLEN             32	            //!< Seed Length in Bytes

#define TOF_NUM_RFC_BURST_PKTS  32	            //!< Maximun number of packets in RF burst

#define TOF_COMPENSATION_NO_COMP         0	    //!< don't use compensation
#define TOF_COMPENSATION_STIM_COMP       1      //!< use STIM compensation (experimental feature)
#define TOF_COMPENSATION_CORRELATOR_COMP 2      //!< use correlator compensation

// If using partial result readout or double sync word buffer, these are the event ID's
#define IRQN_PARTIAL_READ_READY             15  //!< Partial result readout is ready
#define IRQN_DOUBLE_SYNCWORD_BUFFER_SWITCH  11  //!< Same as RF_EventTxBufferChange, use with care
#define RF_EventPartialReadReady            (1 << IRQN_PARTIAL_READ_READY) //!< Partial result readout event bitmap
#define RF_EventDoubleSyncWordBufferSwitch  (1 << IRQN_DOUBLE_SYNCWORD_BUFFER_SWITCH) //!< Double syncword buffer switch event bitmap

#define TOF_FILL_BOTH_BUFF      TOF_SEC_FILL_BOTH_BUFFS   //!< Fill both buffers
#define TOF_FILL_1ST_BUFF       TOF_SEC_FILL_1ST_BUFF     //!< Fill first buffers
#define TOF_FILL_2ND_BUFF       TOF_SEC_FILL_2ND_BUFF     //!< Fill second buffers

// The ready-flags for the double buffers
#define syncWordBufferOneReady 0x01    //!< First buffer ready
#define syncWordBufferTwoReady 0x02    //!< Second buffer ready

#define TICK_TO_METER (6.25 * 3)       //!< Clock tics to distance in meters conversion

#define TOF_PATCH_LOCATION        0x21000914 //!< TOF patch location in memory
#define SUCCESS                   0x00 //!< SUCCESS
#define FAILURE                   0x01 //!< Failure

/// @cond NODOC
#ifdef USE_TRACER
/* tracer configuration */
#define CMD_ENABLE_DBG_CMDID    0x602
#define CMD_ENABLE_DBG_CONFIG   ((0<<14) | (1<<12) | (1<<11) | (2<<9) | (1<<8) | (1<<6))
#endif // USE_TRACER
/// @endcond // NODOC

/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * TYPEDEFS
 */

/** @defgroup TOF_Structs TOF Structures
 * @{
 */

/// @brief Enumeration for TOF roles of operation
typedef enum
{
    ToF_ROLE_SLAVE = 0,
    ToF_ROLE_MASTER,
    ToF_ROLE_PASSIVE
} ToF_Role;

/// @brief TOF Sample
typedef rfc_CMD_TOF_sample_t ToF_Sample;

/// @brief Pointer to frequency calibration struct, used for the pre-calibration of the synth
typedef rfc_CMD_TOF_synthCal_t ToF_SynthCal;

typedef uint8_t* ToF_TX_Buf;  //!< Pointer to TX buffer
typedef uint8_t* ToF_RX_Buf;  //!< Pointer to RX buffer

/// @brief Callback to a handler supplied by the application
typedef void (*TOF_ApplicationCB)(void);
/// @cond NODOC

/// @brief TOF Handle Structure
typedef struct
{
    RF_Object               rfObject;                     //!< Stores the client's internal configuration and states.
    RF_Handle               rfHandle;                     //!< Handle returned by to RF_open
    ToF_Role                tofRole;                      //!< Slave or master
    uint8_t                 numFreqs;                     //!< Number of Frequencies
    uint16_t                *pFrequencies;                //!< Pointer to Frequencies
    uint8_t                 freqChangePeriod;             //!< Period of frequency change in packets. 1 = change frequency between each ping/ack
    ToF_SynthCal            synthCal[TOF_MAX_NUM_FREQ];   //!< Slave or master
    ToF_TX_Buf              pTxBuf;                       //!< Pointer to Tx buffer
    ToF_RX_Buf              pRxBuf;                       //!< Pointer to Rx buffer
    ToF_Sample              *pT1RSSIBuf;                  //!< Pointer to T1|RSSI result buffer
    uint16_t                numBurstSamples;              //!< Number of bursts to be made (same as sync word length if using single buffer)
} ToF_Object, ToF_Struct, *ToF_Handle;
/// @endcond // NODOC

/// @brief TOF Parameters Structure
typedef struct
{
    ToF_Role                tofRole;                      //!< Slave or master
    ToF_Sample              *pT1RSSIBuf;                  //!< Pointer to T1|RSSI result buffer
    uint16_t                numBurstSamples;              //!< Number of bursts to be made (same as sync word length if using single buffer)
    uint8_t                 *pTxBuf;                      //!< Pointer to Tx buffer
    uint8_t                 *pRxBuf;                      //!< Pointer to Rx buffer
    uint16_t                *pFrequencies;                //!< Pointer to Frequencies
    uint8_t                 numFreq;                      //!< Number of frequencies in above list
    tofSecCfgPrms_t         tofSecurityParams;            //!< TOF Security Configuration Paramaters
    uint8_t                 mask;                         //!< Mask for the LFSR
    uint8_t                 freqChangePeriod;             //!< How often should we change freq?
    uint16_t                syncTimeout;                  //!< How long to wait for first sync word
    TOF_ApplicationCB       pfnTofApplicationCB;          //!< Callback to application
    uint8_t                 constSyncwords;               //!< Generate syncwords only once
} ToF_Params;

/// @brief TOF Burst Stats Structure
typedef struct
{
    uint16_t freq;                        //!< Frequency
    double   tick;                        //!< Clock's time between ticks
    double   tickVariance;                //!< Variance of tick value
    uint32_t numOk;                       //!< Num of Ok Recieved
} ToF_BurstStat;

/// @brief TOF Burst Stats Structure
typedef struct
{
    uint32_t     freq;                    //!< Frequency
    RF_EventMask rfEvent;                 //!< RF Event
    uint32_t     ratSyncTime;             //!< RAT Sync Time
} ToF_Status;
/** @} End TOF_Structs */

/*-------------------------------------------------------------------
 * FUNCTIONS
 */

/**
 * @brief   Initiate RF ToF params and open RF driver
 *
 * @param   tofStruct    - ToF structure initiated by user application
 * @param   params - User provided params
 *
 * @return  ToF Handle - Pointer to a ToF_Object
 */
ToF_Handle TOF_open(ToF_Struct *tofStruct, ToF_Params *params);

/**
 * @brief   Start a ToF run
 *
 * @param   handle - ToF Handle.
 * @param   nextTaskTime - Time remaining until next Stack event.
 *
 * @return  ToF Status
 */
ToF_Status TOF_run(ToF_Handle handle, uint32_t nextTaskTime);

/**
 * @brief   Clean buffers for the specified ToF Handle
 *
 * @param   handle - ToF Handle.
 *
 * @return  none
 */
void TOF_clearBuffers(ToF_Handle handle);

/**
 * @brief   Get the result for the last ToF burst (run)
 *
 * @param   handle - ToF Handle.
 * @param   resultBuffer  - The ToF stats for the last run.
 *
 * @return  none
 */
void TOF_getBurstStat(ToF_Handle handle, ToF_BurstStat **resultBuffer);

/**
 * @brief   Close the ToF and RF drivers
 *
 * @param   handle - ToF Handle.
 *
 * @return  none
 */
void TOF_close(ToF_Handle handle);

/**
* @brief   call ToF Security module to generate a true 128-bit random seed
*
* @return  None
*/
uint8_t TOF_genSeed(void);

/**
* @brief   call ToF Security module to restart the ctr drbg
*          new 128-bit random seed
*
* @param   newSeed - New seed handle.
*
* @return  None
*/
uint8_t TOF_setSeed(uint8_t* newSeed);

/**
* @brief   copy 32 byte seed into dst
*
* @param   dst - destination to copy seed into.
*
* @return  None
*/
uint8_t TOF_getSeed(uint8_t *dst);

/**
* @brief    Init sync words buffer, depends on user configuration
*
* @return  None
*/
uint8_t TOF_initSyncWords(void);

/**
* @brief   Skip some syncwords into the future (for synchronization purposes)
*
* @return  None
*/
void TOF_skipSyncWords(void);

/* For frequency calibration: */
#define CMD_READ_FS_CAL 0x000D //!< Read frequency calibration command

/// @brief RF Read frequency calibration command Structure
struct __RFC_STRUCT rfc_CMD_READ_FS_CAL_s 
{
    uint16_t commandNo; //!< The command ID number 0x0601
    uint8_t __dummy0; //!< Reserved
    uint8_t coarseCal; //!< Coarse calibration result
    uint8_t midCal; //!< Mid calibration result
    uint8_t ktCal; //!< KT calibration result
    uint16_t tdcCal; //!< TDC calibration result
};
typedef struct __RFC_STRUCT rfc_CMD_READ_FS_CAL_s rfc_CMD_READ_FS_CAL_t; //!< RF Read frequency calibration command Structure

/*********************************************************************
*********************************************************************/

#ifdef __cplusplus
}
#endif

#endif /* TOF_H_ */

/** @} End TOF */
