/******************************************************************************

 @file  rfc_cmd_tof.h

 @brief This file contains methods to rf commands used by TOF module
 Group: WCS, BTS
 Target Device: cc2640r2

 ******************************************************************************
 
 Copyright (c) 2018-2019, Texas Instruments Incorporated
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:

 *  Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

 *  Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.

 *  Neither the name of Texas Instruments Incorporated nor the names of
    its contributors may be used to endorse or promote products derived
    from this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 ******************************************************************************
 
 
 *****************************************************************************/

/**
 *  @defgroup TOF TOF
 *  @brief This module implements the Time of Flight (TOF)
*
 *  @{
 *  @file  rfc_cmd_tof.h
 *  @brief      rf commands used by TOF module
 */


#ifndef RFC_CMD_TOF_H_
#define RFC_CMD_TOF_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <ti/devices/DeviceFamily.h>
#include DeviceFamily_constructPath(driverlib/rf_mailbox.h)
#include DeviceFamily_constructPath(driverlib/rf_common_cmd.h)
#include DeviceFamily_constructPath(driverlib/rf_prop_cmd.h)

#include "tof/TOF.h"

#include <stdint.h>

typedef struct
{
    int32_t T1; // = [31..8].[7..0], T1 = time << 8 + compensation.
    uint8_t freqIndex;
    int8_t RSSI;
} rfc_CMD_TOF_sample_t;

//Maximum number of different frequencies allowed
typedef struct
{
    uint16_t freq;      // The frequency to which this calibration data is valid
    uint8_t  mid;       // Mid calibration data
    uint8_t  coarse;    // Coarse calibration value
    uint8_t  kt;        // TDC calibration kt result
    uint16_t tdc;       // TDC calibration inverse kt result
} rfc_CMD_TOF_synthCal_t;

typedef struct {
   uint16_t commandNo;                  //!<        The command ID number 0x0811
   uint16_t status;                     //!< \brief An integer telling the status of the command. This value is
                                        //!<        updated by the radio CPU during operation and may be read by the
                                        //!<        system CPU at any time.
   rfc_radioOp_t *pNextOp;              //!<        Pointer to the next operation to run after this operation is done
   ratmr_t startTime;                   //!<        Absolute or relative start time (depending on the value of <code>startTrigger</code>)
   struct {
      uint8_t triggerType:4;            //!<        The type of trigger
      uint8_t bEnaCmd:1;                //!< \brief 0: No alternative trigger command<br>
                                        //!<        1: CMD_TRIGGER can be used as an alternative trigger
      uint8_t triggerNo:2;              //!<        The trigger number of the CMD_TRIGGER command that triggers this action
      uint8_t pastTrig:1;               //!< \brief 0: A trigger in the past is never triggered, or for start of commands, give an error<br>
                                        //!<        1: A trigger in the past is triggered as soon as possible
   } startTrigger;                      //!<        Identification of the trigger that starts the operation
   struct {
      uint8_t rule:4;                   //!<        Condition for running next command: Rule for how to proceed
      uint8_t nSkip:4;                  //!<        Number of skips + 1 if the rule involves skipping. 0: same, 1: next, 2: skip next, ...
   } condition;
   uint16_t __dummy0;

   uint32_t endTime;                    // Timeout [0.250µs ticks] for each measurement
   uint16_t numIterations;              // Number of unique sync words that should be sent
   uint8_t bMaster;                     // Boolean to decide if device is master(1), passive(2) or slave (0)
   uint8_t mode;                        // 0 = 2 Mbps mode, 1 = 1 Mbps mode (2 Mbps recommended)

   uint8_t *pBufTx;                     // Pointer to TX buffer. RF core will blindly read numIterations or doubleBufferLen packets from this pointer. If NULL, it it not used.
   uint8_t *pBufRx;                     // Pointer to RX buffer. RF core will blindly write numIterations or doubleBufferLen packets from this pointer. If NULL, it is not used.
   rfc_CMD_TOF_sample_t* pT1RSSIBuf;    // Contains the time measured, RSSI and frequency
   uint32_t SW0Timestamp;               // RAT timestamp for when SW[0] was found

   uint64_t *pSyncWord;                 // Pointer to sync word buffer 1, this is always used
   uint64_t *pSyncWord2;                // Pointer to sync word buffer 2, only used in double buffer
   uint32_t doubleBufferLen;            // Length (in syncwords) of double buffer. If doubleBufferLen = 0, then single buffer is used, i.e. size found from numIterations

   uint8_t bSync32;                     // Length of syncword, 1 = 32 bit or 2 = 64 bit (64 bit recommended)
   uint8_t txPower;                     // Output Power
   uint8_t bPreamble;                   // Boolean deciding 2 byte preamble (bPreamble2=0 is 1 byte, bPreamble2=1 is 2 byte)
   uint8_t useCompensation;             // If 1 then use STIM, if 2 then correlator compensation is used, else it's unused.

   uint16_t missedPktLimit;             // Limit for missed packets. 0xFFFF is run forever IF missedSW0Limit == 0xFFFF
   uint16_t missedSW0Limit;             // Limit for missed SW[0]. When this is reached, command will return. 0xFFFF is run forever if missedPktLimit = 0xFFFF
   uint16_t timeslotTimeout;            // Timeslots timeout, in [0.250?s ticks]. Needs to be larger than time for transmission + timeout.
   uint16_t syncTimeout;                // How long first RX timeout should be in [0.250?s ticks]  ticks. This is to enable a longer RX time for the first syncword

   uint8_t numFreq;                     // Number of frequencies used in the frequency hopping
   uint8_t seed;                        // Seed for random number generator used for frequency hopping
   uint8_t mask;                        // Mask for random number generator used for frequency hopping
   uint8_t period;                      // Period for how often freq. should change, period in regards to sync words

   rfc_CMD_TOF_synthCal_t* freqCalibList;// Pointer to frequency calibration struct, used for the pre-calibration of the synth

   uint16_t freqCalibTime;              // Time for frequency calibration in [0.250?s ticks], in the API due to calibration settings
   uint16_t numResultsReady;            // Variable telling how many results have been written to the result buffer. To be used with the partial result readout

   uint16_t resultReadyFrequency;       // The frequency of which results are reported, where results are reported when each n*resultReadyFrequency results were collected
   uint8_t syncwordBufferInUse;         // Flag to say which of the two syncword buffer are in use.
   uint8_t syncwordBufferReady;         // Flag to say which of and if the double buffers are ready to be used.
   
   uint16_t txRxBufferLen;				// Length of tx/rx payload buffer in bytes.
   uint8_t LQIThreshold;				// Threshold for filtering the magnitude of LQI. Will filter out slave responses in master node, and slave and master responses in passive node. Lower threshold will filter more.
   uint8_t magnDiffThreshold;			// Threshold for magnitude difference in standard packet (when tx/rx buffer values are not applied). Lower threshold will filter more.
   
} rfc_CMD_TOF_t;

#ifdef __cplusplus
}
#endif

#endif /* RFC_CMD_TOF_H_ */

/** @} End TOF */
