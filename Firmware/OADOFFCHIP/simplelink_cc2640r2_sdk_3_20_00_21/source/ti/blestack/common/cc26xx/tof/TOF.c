/*
 * Copyright (c) 2017-2018, Texas Instruments Incorporated
 * All rights reserved.
 *
 * IMPORTANT: Your use of this Software is limited to those specific rights
 * granted under the terms of a software license agreement between the user
 * who downloaded the software, his/her employer (which must be your employer)
 * and Texas Instruments Incorporated (the "License"). You may not use this
 * Software unless you agree to abide by the terms of the License. The License
 * limits your use, and you acknowledge, that the Software may not be modified,
 * copied or distributed unless embedded on a Texas Instruments microcontroller
 * or used solely and exclusively in conjunction with a Texas Instruments radio
 * frequency transceiver, which is integrated into your product. Other than for
 * the foregoing purpose, you may not use, reproduce, copy, prepare derivative
 * works of, modify, distribute, perform, display or sell this Software and/or
 * its documentation for any purpose.
 *
 * YOU FURTHER ACKNOWLEDGE AND AGREE THAT THE SOFTWARE AND DOCUMENTATION ARE
 * PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY, TITLE,
 * NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL
 * TEXAS INSTRUMENTS OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER CONTRACT,
 * NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR OTHER
 * LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
 * INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE
 * OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT
 * OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
 * (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *
 * Should you have any questions regarding your right to use this Software,
 * contact Texas Instruments Incorporated at www.TI.com.
 *
 */

#include <TOF.h>

// Tof Patches
#include <ti/devices/DeviceFamily.h>
#include DeviceFamily_constructPath(rf_patches/rf_patch_mce_tof.h)
#include DeviceFamily_constructPath(rf_patches/rf_patch_rfe_tof.h)

#if defined(CC26X2)
#include DeviceFamily_constructPath(rf_patches/rf_patch_cpe_multi_protocol_rtls.h)
#elif defined(CC26XX_R2)
#include DeviceFamily_constructPath(rf_patches/rf_patch_cpe_tof.h)
#endif

#include DeviceFamily_constructPath(inc/hw_rfc_dbell.h)

#include "rfc_cmd_tof.h"
#include <ti/drivers/rf/RF.h>
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/hal/Hwi.h>
#include <ti/sysbios/knl/Swi.h>

#include "rtls_ctrl.h"

#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>

#define USE_TRACER

// TI-RTOS RF Mode Object
RF_Mode RF_Mode_tof =
{
  .rfMode = RF_MODE_PROPRIETARY,
#if defined(CC26X2)
  .cpePatchFxn = &rf_patch_cpe_multi_protocol_rtls,
#elif defined(CC26XX_R2)
  .cpePatchFxn = &rf_patch_cpe_tof,
#endif
  .mcePatchFxn = &rf_patch_mce_tof,
  .rfePatchFxn = &rf_patch_rfe_tof,
};

static uint32_t shape[6] = {
    ((19  << 12) | ( 410 )), // 500 ns pulse
    0,
    0,
    0,
    0,
    0,
};

#if defined(CC26XX)

#if defined(CC26XX_R2)
// Override list for the fast synth settings:
// Overrides for CMD_RADIO_SETUP
uint32_t pOverrides_2M[] =
{
  MCE_RFE_OVERRIDE(1,0,0,1,0,0),
  ADI_HALFREG_OVERRIDE(0,61,0xF,0x0),
  // Force gain setting
  HW_REG_OVERRIDE(0x6084,0x3588),
  // Rx: Set LNA IB trim value based on the selected defaultPhy.mainMode setting. (NOTE: The value 0x8 is a placeholder. The value to use should be set during run-time by radio driver function.)
  ADI_HALFREG_OVERRIDE(0,4,0xF,0x8),
  // Shaper settings:
  // Enable shaper, gain = 0, use shaped 15.4 modulation
  (uint32_t)0x00810083,
  // Force mdmTxIf = 0 on synth calibration
  (uint32_t)0x00000343,
  // Force mdmRxIf = 0 on synth calibration
  (uint32_t)0x00000323,
  // Set TX IF to zero and no dynamic TX IF calculation
  (uint32_t)0x000000a3,
  // Place the pointer at correct place in command
  (uint32_t)0xC0040031,
  // Pointer to the shape
  (uint32_t)shape,

  // Fast synth settings:
  // Synth: Use 24 MHz crystal, enable extra PLL filtering
  (uint32_t)0x02010403,
  // Synth: Set fine top and bottom code to 127 and 0
  HW_REG_OVERRIDE(0x4020, 0x7F00),
  // Synth: Configure faster calibration
  HW32_ARRAY_OVERRIDE(0x4004, 1),
  // Synth: Configure faster calibration
  (uint32_t)0x1C0C0618,
  // Synth: Configure faster calibration
  (uint32_t)0xC00401A1,
  // Synth: Configure faster calibration
  (uint32_t)0x21010101,
  // Synth: Configure faster calibration
  (uint32_t)0xC0040141,
  // Synth: Configure faster calibration
  (uint32_t)0x00214AD3,
  // Synth: Decrease synth programming time-out by 90 us (0x0298 RAT ticks = 166 us)
  (uint32_t)0x02980243,
  // End of fast synth settings

  // Set the correlator threshold to 0x68, default is 0x5A
  HW_REG_OVERRIDE(0x5108, 0x0068),
  // Set correlator for pre-correlation to 0x2A, default is 0x3737
  HW_REG_OVERRIDE(0x5104, 0x2A2A),

#ifdef CACHE_AS_RAM
  0x00018063,
#endif //CACHE_AS_RAM

  // End of overrides
  (uint32_t)0xFFFFFFFF,
};

#elif defined(CC26X2)
uint32_t pOverrides_2M[] =
{
  MCE_RFE_OVERRIDE(1,0,0,1,0,0),
  HW_REG_OVERRIDE(0x6098,0x3588), // Force gain setting
  (uint32_t)0x00000343, // Force mdmTxIf = 0
  (uint32_t)0x00000323, // Force mdmRxIf = 0, will cause the synth to take into account when programming
  (uint32_t)0x000000a3, // Set TX IF to zero and no dynamic TX IF calculation
  (uint32_t)0x00810083, // Enable shaper, gain = 0, use shaped 15.4 modulation
  (uint32_t)0xC0040031, // Place the pointer at correct place in command
  (uint32_t)shape,      // Pointer to the shape
  HW_REG_OVERRIDE(0x5118, 0x0068), // default = 0x005A
  HW_REG_OVERRIDE(0x5114, 0x2A2A), // default = 0x3737
  //If we want to shorten the time for slave turnaround, we can mess with this override, writing to BRMACC0. turnaround is this value at 24 MHz
  HW_REG_OVERRIDE(0x5264, 0x0340), // DEFAULT: 0x190

  (uint32_t)0xFFFFFFFF,
};
#endif// <board>

#endif //CC26XX

rfc_CMD_SYNC_START_RAT_t RF_cmdStartRAT =
{
 .commandNo = 0x080A,
 .status = 0x0000,
 .pNextOp = 0,                        // INSERT APPLICABLE POINTER: (uint8_t*)&xxx
 .startTime = 0x00000000,
 .startTrigger.triggerType = 0x0,
 .startTrigger.bEnaCmd = 0x0,
 .startTrigger.triggerNo = 0x0,
 .startTrigger.pastTrig = 0x0,
 .condition.rule = 0x1,
 .condition.nSkip = 0x0,

 .__dummy0 = 0x00,
 .rat0 = 0x0000,
};

// CMD_RADIO_SETUP
rfc_CMD_RADIO_SETUP_t RF_cmdRadioSetup =
{
  .commandNo = 0x0802,
  .status = 0x0000,
  .pNextOp = (rfc_radioOp_t*)&RF_cmdStartRAT,
  .startTime = 0x00000000,
  .startTrigger.triggerType = 0x0,
  .startTrigger.bEnaCmd = 0x0,
  .startTrigger.triggerNo = 0x0,
  .startTrigger.pastTrig = 0x0,
  .condition.rule = 0x1,
  .condition.nSkip = 0x0,
  .mode = 0x02,
#if defined(CC26XX_R2)
  .__dummy0 = 0x00,
#endif
  .config.frontEndMode = 0x0,
  .config.biasMode = 0x0,
  .config.analogCfgMode = 0x0,
  .config.bNoFsPowerUp = 0x0,
  .txPower = 0x9330,
  .pRegOverride = 0,
};

// CMD_TOF
rfc_CMD_TOF_t RF_cmdTof =
{
  .commandNo = 0x0811,
  .status = 0x0000,
  .pNextOp = 0,
  .startTime = 0x00000000,
  .startTrigger.triggerType = 0x0,
  .startTrigger.bEnaCmd = 0x0,
  .startTrigger.triggerNo = 0x0,
  .startTrigger.pastTrig = 0x0,
  .condition.rule = 0x1,
  .condition.nSkip = 0x0,
};

// CMD_FS
rfc_CMD_FS_t RF_cmdFs =
{
  .commandNo = 0x0803,
  .status = 0x0000,
  .pNextOp = 0,
  .startTime = 0x00000000,
  .startTrigger.triggerType = 0x0,
  .startTrigger.bEnaCmd = 0x0,
  .startTrigger.triggerNo = 0x0,
  .startTrigger.pastTrig = 0x0,
  .condition.rule = 0x1,
  .condition.nSkip = 0x0,
  .frequency = 2432,
  .fractFreq = 0x0000,
  .synthConf.bTxMode = 0x1,
  .synthConf.refFreq = 0x0,
  .__dummy0 = 0x00,
  .__dummy1 = 0x00,
  .__dummy2 = 0x00,
  .__dummy3 = 0x0000,
};


// Immediate command to read calibration settings
rfc_CMD_READ_FS_CAL_t RF_cmdReadFsCal =
{
  .commandNo = 0x000D,
  .__dummy0 = 0,
  .coarseCal = 0,
  .midCal = 0,
  .ktCal = 0,
  .tdcCal = 0
};

// Last RF event mask
uint32_t g_lastRfHwiEventMask;

// ToF application callback - will be called when a ToF run is complete (all syncwords exhausted)
TOF_ApplicationCB tofApplicationCB = NULL;

// RF Callback - RF Driver will call this function
RF_Callback pRfCallbackFxn = NULL;

// Security handle for tof_security module
tofSecHandle_t gTofSecHandle;

// Holds the last buffer that was filled (in case ToF Security is using double buffer mode)
uint8_t gLastBuffFilled = 0;

// Keeps track of the amount of buffer switches needed to complete the run
// Since syncwords can be generated numerous times when using double buffer we need to track
// how many are needed to complete the ToF run
uint8_t gBufferSwitchRemaining = 0;

// Holds information whether we managed to perform a full ToF run at least once
// This parameter is reset by rtls_ctrl every time ToF is disabled
extern uint8_t bFirstTimeSync;

// Tells us if we should generate words or generate once and keep using those until told otherwise
uint8_t gConstSyncwords = FALSE;

void TOF_calibrateSynth(ToF_Handle handle, uint16_t *freqList, uint8_t numFreq);
void TOF_tofCallback(RF_Handle h, RF_CmdHandle ch, RF_EventMask e);
void TOF_runImmediateWrapperFs(RF_Handle h, RF_CmdHandle ch, RF_EventMask e);
uint8_t TOF_genSyncWords(uint8_t bufferToFill);

/*********************************************************************
 * @fn      TOF_tofCallback
 *
 * @brief   Callback for RF
 *
 * @param   h  - RF Handle.
 * @param   ch - RF Command Handle.
 * @param   e  - RF Event mast.
 *
 * @return  none
 */
void TOF_tofCallback(RF_Handle h, RF_CmdHandle ch, RF_EventMask e)
{
  g_lastRfHwiEventMask = e;

  if (e & RF_EventRxOk)
  {    
    if ((tofApplicationCB != NULL))
    {
      bFirstTimeSync = 1;
      tofApplicationCB();
      
      // This run was successful generate initial words for next run
      if (gTofSecHandle.tofSecCfgParams.bUseDoubleBuffer == TOF_MODE_DBL_BUF)
      {
        // Set the value for amount of switches for the next run
        gBufferSwitchRemaining = RF_cmdTof.numIterations/(gTofSecHandle.numOfSyncWordsPerBuffer);

        if (2 == gLastBuffFilled)
        {
          TOF_genSyncWords(TOF_SEC_FILL_1ST_BUFF);
          gLastBuffFilled = 1;
        }
        else if (1 == gLastBuffFilled)
        {
          TOF_genSyncWords(TOF_SEC_FILL_2ND_BUFF);
          gLastBuffFilled = 2;
        }
      }
      else
      {
        TOF_genSyncWords(TOF_SEC_FILL_1ST_BUFF);
      }
    }
  }
  else if (e & RF_EventDoubleSyncWordBufferSwitch)
  { 
    if (gBufferSwitchRemaining != 0)
    {
      gBufferSwitchRemaining--;
    }

    // If the RF core is not currently using buffer 2, OK to fill it
    if ((RF_cmdTof.syncwordBufferInUse & 0x02) == 0)
    {
      // Using sync word buffer 1 currently, fill the second buffer
      TOF_genSyncWords(TOF_SEC_FILL_2ND_BUFF);

      gLastBuffFilled = 2;
    }
    // Else if the RF core is not currently using buffer 1, OK to fill it
    else if ((RF_cmdTof.syncwordBufferInUse & 0x01) == 0)
    {
      // Using sync word buffer 2 currently, fill the first buffer
      TOF_genSyncWords(TOF_SEC_FILL_1ST_BUFF);

      gLastBuffFilled = 1;
    }
  }
  else if (e & RF_EventRxNOk)
  {
    TOF_skipSyncWords();
  }
}


/*********************************************************************
 * @fn      TOF_open
 *
 * @brief   Initiate RF ToF params and open RF driver
 *
 * @param   tof    - ToF structure initiated by user application
 * @param   params - User provided params
 *
 * @return  ToF Handle - Pointer to a ToF_Object
 */
ToF_Handle TOF_open(ToF_Struct *tof, ToF_Params *params)
{
  // TOF driver re-opened, we are not in sync
  bFirstTimeSync = 0;

  // Default values
  params->syncTimeout      = 2500*4;
  params->freqChangePeriod = 2;      // Switch frequency for each 2 packets
  params->pRxBuf           = NULL;
  params->pTxBuf           = NULL;

  tof->freqChangePeriod    = params->freqChangePeriod;
  tof->pFrequencies        = params->pFrequencies;
  tof->numBurstSamples     = params->numBurstSamples;
  tof->numFreqs            = params->numFreq;
  tof->pT1RSSIBuf          = params->pT1RSSIBuf;
  tof->tofRole             = params->tofRole;
  tof->pTxBuf              = params->pRxBuf;
  tof->pRxBuf              = params->pTxBuf;

  gConstSyncwords          = params->constSyncwords;

  // need to reinitialize these parameters to generate new sync words after reset when using constSyncwords
  gLastBuffFilled          = 0;
  gBufferSwitchRemaining   = 0;

  // ToF Security
  if (TOF_SEC_SUCCESS_E != TOFSecurity_open(&gTofSecHandle, &(params->tofSecurityParams)))
  {
    return NULL;
  }

  // Initialize
  RF_Params rfParams;
  RF_Params_init(&rfParams);
  RF_cmdTof = (rfc_CMD_TOF_t){0};                                                              // Clear all values

  rfParams.nPowerUpDuration      = 4500;
  rfParams.nInactivityTimeout    = BIOS_WAIT_FOREVER;

  // User configurable
  RF_cmdTof.syncTimeout          = params->syncTimeout;                                        // Timeout 0.250us ticks for first sync word. In order for RF core to stay in RX at first for longer
  RF_cmdTof.pT1RSSIBuf           = params->pT1RSSIBuf;                                         // Pointer to result buffer
  RF_cmdTof.numIterations        = params->numBurstSamples;                                    // Number of iterations of the sync word buffer. Since sync word is changed between ping/ack, it needs to be double the amount of packets.
  RF_cmdTof.missedPktLimit       = 32;                                                         // Limit for missed packets. Reached limit will cause RF core to return
  RF_cmdTof.pBufTx               = params->pTxBuf;                                             // Pointer to Tx buffer. If NULL then the buffer will be ignored by the RF core.
  RF_cmdTof.pBufRx               = params->pRxBuf;                                             // Pointer to Rx buffer. If NULL then the buffer will be ignored by the RF core.
  RF_cmdTof.pSyncWord            = (uint64_t *)gTofSecHandle.pSyncWordBuffer1;                 // Pointer to sync words
  RF_cmdTof.pSyncWord2           = (uint64_t *)gTofSecHandle.pSyncWordBuffer2;                 // Pointer to the double buffer
  RF_cmdTof.bMaster              = params->tofRole;                                            // 2 -> Passive ,1 -> Master, 0 -> Slave
  RF_cmdTof.period               = params->freqChangePeriod;                                   // Period of frequency change in packets. Should be at least 2.
  RF_cmdTof.missedSW0Limit       = 16;                                                         // Limit for number of missed first packets
  RF_cmdTof.resultReadyFrequency = 0xFFFF;                                                     // Partial read out not used here, set the partial readout to occur very(!) seldom -> no partial readout
  RF_cmdTof.useCompensation      = TOF_COMPENSATION_CORRELATOR_COMP;
  RF_cmdTof.txRxBufferLen        = 0xFFFF;                                                        // Set buffer length to max. Needs to be set in accordance to pTxBuf and pRxBuf length.
  RF_cmdTof.LQIThreshold         = 20;                                                          // Set LQI threshold to 20. This will cause packets with LQI under 20 to mark as timeout and thereby unused. 255 = no filtering
  RF_cmdTof.magnDiffThreshold    = 255;                                                         // Set magnDiffThreshold to unused. This is a secondary filtering to LQI, where 0 is no difference between 0's/1's in packet, 100 = 100% difference i.e. magn(0) can be twice of magn(1). This only applies for packets sent without defined payload, i.e. if measurement iterator > txRxBufferLen or pTxBuf = NULL.

  // Constant configuration
  RF_cmdTof.commandNo            = 0x6801;
  RF_cmdTof.condition.rule       = COND_NEVER;                                                 // When to run next command
  RF_cmdTof.endTime              = 0;                                                          // Timeout per transmission set individually for slave and master below!
  RF_cmdTof.txPower              = 0x3f;                                                       // MAX output power, not temperature compensated!
  RF_cmdTof.bPreamble            = 0;                                                          // 0 -> 1 byte preamble, 1 -> 2 byte preamble
  RF_cmdTof.mode                 = 0;                                                          // 1 -> 1Mbps mode (not supported), 0 -> 2Mbps mode (Recommended)
  RF_cmdTof.timeslotTimeout      = 400*4;                                                      // Timeslot delay. should be enough to also include timeouts.
  RF_cmdTof.bSync32              = 0;                                                          // 0 = 64 bit sync word (recommended), 1 = 32 bit sync word(not supported)
  RF_cmdTof.seed                 = 0;                                                          // Seed for LFSR random frequency hopping 0 = don't use LFSR, iterate linearly instead
  RF_cmdTof.mask                 = 0x59;                                                       // Mask for LFSR random frequency hopping.
  RF_cmdTof.freqCalibTime        = 105*4;                                                      // Calibration time for the fast frequency change. Will be different depending on freq calib settings. Incluing LFSR calculation time


  // Double buffer initialization
  if (gTofSecHandle.tofSecCfgParams.bUseDoubleBuffer)
  {
    RF_cmdTof.doubleBufferLen = gTofSecHandle.numOfSyncWordsPerBuffer; // Double buffer used - Get the length of the double buffer
  }
  else // Double buffer NOT used - length is set to 0
  {
    RF_cmdTof.doubleBufferLen = 0;
  }
  
  // Conf 2M override
  RF_cmdRadioSetup.pRegOverride = pOverrides_2M;
  RF_cmdRadioSetup.mode = 2;

  // Set up the RF commands depending on master/slave/passive
  if (RF_cmdTof.bMaster == ToF_ROLE_MASTER)
  {
    // How long to wait for sync, in ticks of 0.250us
    #if defined(CC26X2)
        RF_cmdTof.endTime = RF_cmdTof.timeslotTimeout - 70*4;
    #elif defined(CC26XX_R2)
        RF_cmdTof.endTime = RF_cmdTof.timeslotTimeout - 50*4;
    #endif
  }
  else if (RF_cmdTof.bMaster == ToF_ROLE_PASSIVE)
  {
    // How long to wait for sync, in ticks of 0.250us. Could be trimmed to be less!
    RF_cmdTof.endTime = RF_cmdTof.timeslotTimeout - 70*4;
    RF_cmdTof.syncTimeout = 2500*4;
    RF_cmdTof.LQIThreshold = 40;
  }
  else if (RF_cmdTof.bMaster == ToF_ROLE_SLAVE)
  {
    // How long to wait for sync, in ticks of 0.250us.
    RF_cmdTof.endTime = RF_cmdTof.timeslotTimeout - 70*4;
    RF_cmdTof.syncTimeout = 2500*4;
  }

  // Need to keep track of the amount of buffer switches - set initial value
  if (gTofSecHandle.tofSecCfgParams.bUseDoubleBuffer == TOF_MODE_DBL_BUF)
  {
    gBufferSwitchRemaining = RF_cmdTof.numIterations/(gTofSecHandle.numOfSyncWordsPerBuffer);
  }

  /* Open radio driver */
  tof->rfHandle = RF_open(&tof->rfObject, &RF_Mode_tof, (RF_RadioSetup*)&RF_cmdRadioSetup, &rfParams);

  if (tof->rfHandle)
  {
    if (params->pfnTofApplicationCB != NULL)
    {
      tofApplicationCB = params->pfnTofApplicationCB;
    }

    pRfCallbackFxn = TOF_tofCallback;

    return (ToF_Handle)tof;
  }
  else
  {
      return NULL;
  }
}

/*********************************************************************
 * @fn      TOF_run
 *
 * @brief   Start a ToF run
 *
 * @param   handle - ToF Handle.
 * @param   nextTaskTime - Time remaining until next Stack event.
 *
 * @return  ToF Status
 */
ToF_Status TOF_run(ToF_Handle handle, uint32_t nextTaskTime)
{
  RF_ScheduleCmdParams cmdParams =
  {
    0,                  // Ignore endTime
    RF_PriorityHighest, // Highest priority
    RF_AllowDelayAny    // Don't care delay
  };
  ToF_Status ret = {0};
  
  RF_cmdTof.numFreq = handle->numFreqs;

  // Run frequency calibration if needed
  for (int i = 0; i < handle->numFreqs; ++i)
  {
    if (handle->synthCal[i].freq != handle->pFrequencies[i])
    {
      TOF_calibrateSynth(handle, handle->pFrequencies, handle->numFreqs);
      break;
    }
  }

  // Schedule the ToF run
  RF_cmdTof.startTime = RF_getCurrentTime() + RF_convertUsToRatTicks(1500);
  RF_cmdTof.startTrigger.triggerType = TRIG_ABSTIME;
  RF_cmdTof.startTrigger.pastTrig = 0;

  if (RF_cmdTof.bMaster == ToF_ROLE_MASTER)
  {
    RF_cmdTof.startTime = RF_cmdTof.startTime + RF_convertUsToRatTicks(2500);
  }

  cmdParams.endTime = RF_cmdTof.startTime + RF_convertUsToRatTicks(nextTaskTime - 5500);

  // Keep track of switch amount
  if (gTofSecHandle.tofSecCfgParams.bUseDoubleBuffer == TOF_MODE_DBL_BUF)
  {
    // Set the value for amount of switches for the next run
    gBufferSwitchRemaining = RF_cmdTof.numIterations/(gTofSecHandle.numOfSyncWordsPerBuffer);
  }

  // Will return immediately
  RF_scheduleCmd(handle->rfHandle, (RF_Op*)&RF_cmdTof, &cmdParams, pRfCallbackFxn, RF_EventRxOk | RF_EventRxNOk | RF_EventDoubleSyncWordBufferSwitch | RF_EventLastCmdDone);

  ret.rfEvent = g_lastRfHwiEventMask,
  ret.ratSyncTime = RF_cmdTof.SW0Timestamp,

  // Reset sync time before next run
  RF_cmdTof.SW0Timestamp = 0;

  return ret;
}

/*********************************************************************
 * @fn      TOF_getBurstStat
 *
 * @brief   Get the result for the last ToF burst (run)
 *
 * @param   handle - ToF Handle.
 * @param   stats  - The ToF stats for the last run.
 *
 * @return  none
 */
void TOF_getBurstStat(ToF_Handle handle, ToF_BurstStat **resultBuffer)
{
  // Get the average
  ToF_Sample *pSample = (ToF_Sample *)handle->pT1RSSIBuf;
  ToF_BurstStat *stats = *resultBuffer;

  // Clean results buffer
  memset(stats, 0, handle->numFreqs * sizeof(ToF_BurstStat));

  for (uint32_t i = 0; i < handle->numBurstSamples / 2; i++, pSample++)
  {
    uint32_t t1 = pSample->T1;

    if (t1 != TOF_TIMEOUTVAL && t1 != 0)
    {
      stats[pSample->freqIndex].tick += pSample->T1 / 256;
      stats[pSample->freqIndex].numOk++;
    }
  }

  for (uint32_t i = 0; i < handle->numFreqs; ++i)
  {
    stats[i].freq = handle->synthCal[i].freq;
    stats[i].tick /= stats[i].numOk;
  }

  // Find the variance
  pSample = (ToF_Sample *)handle->pT1RSSIBuf;
  for (uint32_t i = 0; i < handle->numBurstSamples / 2; i++, pSample++)
  {
    uint32_t t1 = pSample->T1;
    if (t1 != TOF_TIMEOUTVAL && t1 != 0)
    {
      double dev = pSample->T1 / 256 - stats[pSample->freqIndex].tick;
      stats[pSample->freqIndex].tickVariance += dev * dev;
    }
  }

  for (uint32_t i = 0; i < handle->numFreqs; ++i)
  {
    stats[i].tickVariance /= stats[i].numOk;
  }
}

/*********************************************************************
 * @fn      TOF_close
 *
 * @brief   Close the ToF and RF drivers
 *
 * @param   handle - ToF Handle.
 *
 * @return  none
 */
void TOF_close(ToF_Handle handle)
{
  volatile uint32_t keyHwi;

  // Delete synth calibrations
  handle->synthCal[0].freq = 0;

  // Close RF Handle
  RF_close(handle->rfHandle);

  // Close ToF Security
  TOFSecurity_close(&gTofSecHandle);

  // Free Tof Handle
  if (handle->pFrequencies)
  {
    RTLSUTIL_FREE(handle->pFrequencies);
  }

  if (handle->pT1RSSIBuf)
  {
    RTLSUTIL_FREE(handle->pT1RSSIBuf);
  }
}

/*********************************************************************
 * @fn      TOF_clearBuffers
 *
 * @brief   Clean buffers for the specified ToF Handle
 *
 * @param   handle - ToF Handle.
 *
 * @return  none
 */
void TOF_clearBuffers(ToF_Handle handle)
{
  ToF_Sample *pSamples = (ToF_Sample *)handle->pT1RSSIBuf;

  for (int i = 0 ; i < handle->numBurstSamples / 2 ; i++)
  {
    //Reset T1SampleBuf to T1 = timeout value =0xffff | RSSI = 0
    pSamples[i].T1 = TOF_TIMEOUTVAL;
    pSamples[i].RSSI = 0xA;
  }
}

/*********************************************************************
 * @fn      TOF_calibrateSynth
 *
 * @brief   This function is used to pre-calibrate the synth for fast frequency changes.
 *          In order for the fast changing to work, ToF_run must start at the "middle" frequency
 *          so that the minimal offset between outer frequencies and start frequency if obtained.
 *
 * @param   handle   - ToF Handle.
 * @param   freqList - List of frequencies that will be used for a ToF run.
 * @param   numFreq  - Amount of frequencies.
 *
 * @return  none
 */
void TOF_calibrateSynth(ToF_Handle handle, uint16_t *freqList, uint8_t numFreq)
{
  RF_ScheduleCmdParams cmdParams =
  {
    0,                  // Ignore endTime
    RF_PriorityHighest, // Highest priority
    RF_AllowDelayAny    // Don't care delay
  };

  // Allow max TOF_MAX_NUM_FREQ frequencies
  numFreq = numFreq < TOF_MAX_NUM_FREQ ? numFreq : TOF_MAX_NUM_FREQ;

  // Calibrate for all frequencies:
  for (int i = 0 ; i < numFreq ; i++)
  {
    // Set frequency in struct:
    RF_cmdFs.frequency = freqList[i];
    // Run cmd_Fs and wait for done:

    RF_cmdFs.startTime = RF_getCurrentTime();
    RF_cmdFs.startTrigger.triggerType = TRIG_ABSTIME;
    RF_cmdFs.startTrigger.pastTrig = 1;
    cmdParams.endTime = RF_cmdFs.startTime + RF_convertUsToRatTicks(300);

    RF_runScheduleCmd(handle->rfHandle, (RF_Op*)&RF_cmdFs, &cmdParams, &TOF_runImmediateWrapperFs, 0);

    // Copy the results over to struct which is passed to RF core:
    handle->synthCal[i].freq = freqList[i]; // Save the current frequency
    handle->synthCal[i].mid = RF_cmdReadFsCal.midCal;
    handle->synthCal[i].coarse = RF_cmdReadFsCal.coarseCal;
    handle->synthCal[i].kt = RF_cmdReadFsCal.ktCal;
    handle->synthCal[i].tdc = RF_cmdReadFsCal.tdcCal;
  }

  // Provide RF_cmdToF with pointer to calibration data:
  RF_cmdTof.freqCalibList = handle->synthCal;
}

/*********************************************************************
 * @fn      TOF_runImmediateWrapperFs
 *
 * @brief   A wrapper for runImmediateCmd. This routine is used as a callback
 *          to RF_scheduleCmd. Once RF_scheduleCmd is finished, this routine will
 *          be immediately called and RF_runImmediateCmd will be called subsequently.
 *
 * @param   h  - RF Handle.
 * @param   ch - RF Command Handle.
 * @param   e  - RF Event mast.
 *
 * @return  none
 */
void TOF_runImmediateWrapperFs(RF_Handle h, RF_CmdHandle ch, RF_EventMask e)
{
  RF_runImmediateCmd(h, (uint32_t*)&RF_cmdReadFsCal);
}

/*********************************************************************
* @fn      TOF_generateSeed
*
* @brief   call ToF Security module to generate a true 128-bit random seed
*
* @param   handle - ToF Handle.
*
* @return  None
*/
uint8_t TOF_genSeed(void)
{
  return TOFSecurity_genSeed(&gTofSecHandle);
}

/*********************************************************************
* @fn       TOF_GenSyncWords
*
* @brief    Will fill Sync Word Buffers with <numOfSyncWordsPerBuffer> Sync Words
*
* @param    handle - ToF Handle.
*
* @return  None
*/
uint8_t TOF_genSyncWords(uint8_t bufferToFill)
{
  uint8_t res;

  // If we did not yet initialize a buffer and syncwords are constant or if syncwords are not constant
  if ((gLastBuffFilled == 0 && gConstSyncwords == TRUE) || gConstSyncwords == FALSE)
  {
    res = TOFSecurity_genSyncWords(&gTofSecHandle, bufferToFill);
  }
  else // constant syncwords require that we generate only once
  {
    res = TOF_SEC_SUCCESS_E;
  }

  if (TOF_SEC_SUCCESS_E == res)
  {
    switch(bufferToFill)
    {
      case TOF_SEC_FILL_1ST_BUFF:
      {
        RF_cmdTof.syncwordBufferReady |= syncWordBufferOneReady;
      }
      case TOF_SEC_FILL_2ND_BUFF:
      {
        RF_cmdTof.syncwordBufferReady |= syncWordBufferTwoReady;
      }
      case TOF_SEC_FILL_BOTH_BUFFS:
      {
        RF_cmdTof.syncwordBufferReady = syncWordBufferOneReady | syncWordBufferTwoReady;
      }
    }
  }
  return res;
}

/*********************************************************************
* @fn       TOF_initSyncWords
*
* @brief    Init sync words buffer, depends on user configuration
*
* @param    none
*
* @return  None
*/
uint8_t TOF_initSyncWords(void)
{
  uint8_t ret;
   
  // Single buffer mode
  if (TOF_MODE_SINGLE_BUF == gTofSecHandle.tofSecCfgParams.bUseDoubleBuffer)
  {
    ret = TOF_genSyncWords(TOF_SEC_FILL_1ST_BUFF);
    if (TOF_SEC_SUCCESS_E == ret)
    {
      gLastBuffFilled = 1;
    }
  }
  // Double buffer mode
  else
  {
    ret = TOF_genSyncWords(TOF_SEC_FILL_BOTH_BUFFS);
    if (TOF_SEC_SUCCESS_E == ret)
    {
      gLastBuffFilled = 2;
    }
  }
  return ret;
}

/*********************************************************************
* @fn      TOF_reseed
*
* @brief   call ToF Security module to restart the ctr drbg with a 
*          new 128-bit random seed
*
* @param   handle - ToF Handle.
*
* @return  None
*/
uint8_t TOF_setSeed(uint8_t* newSeed)
{
  return TOFSecurity_setSeed(&gTofSecHandle, newSeed);
}

/*********************************************************************
* @fn      TOF_getSeed
*
* @brief   copy 32 byte seed into dst
*
* @param   dst - destination to copy seed into.
*
* @return  None
*/
uint8_t TOF_getSeed(uint8_t *dst)
{
  if(NULL != dst)
  {
    memcpy(dst, &gTofSecHandle.seed, CTR_DRBG_SEEDLEN);
  }
  else
  {
    return FAILURE;
  }
  return SUCCESS;
}

/*********************************************************************
* @fn      TOF_skipSyncWords
*
* @brief   Skip some syncwords into the future (for synchronization purposes)
*
* @param   None
*
* @return  None
*/
void TOF_skipSyncWords(void)
{
  // Check if we synchronized for the first time (if we did not, there's no purpose in skipping)
  if (bFirstTimeSync == 1)
  {
    // If we are in sync, check that we are configured
    if (RF_cmdTof.numIterations != 0)
    {
      // Generate syncwords
      for (int i = 0; i < gBufferSwitchRemaining; i++)
      {
        if (2 == gLastBuffFilled)
        {
          TOF_genSyncWords(TOF_SEC_FILL_1ST_BUFF);
          gLastBuffFilled = 1;
        }
        else if (1 == gLastBuffFilled)
        {
          TOF_genSyncWords(TOF_SEC_FILL_2ND_BUFF);
          gLastBuffFilled = 2;
        }
      }
    }
  }

  // Set the value for amount of switches for the next run
  gBufferSwitchRemaining = RF_cmdTof.numIterations/(gTofSecHandle.numOfSyncWordsPerBuffer);
}
