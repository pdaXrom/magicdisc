/*
 * Copyright (c) 2017-2018, Texas Instruments Incorporated
 * All rights reserved.
 *
 * IMPORTANT: Your use of this Software is limited to those specific rights
 * granted under the terms of a software license agreement between the user
 * who downloaded the software, his/her employer (which must be your employer)
 * and Texas Instruments Incorporated (the "License"). You may not use this
 * Software unless you agree to abide by the terms of the License. The License
 * limits your use, and you acknowledge, that the Software may not be modified,
 * copied or distributed unless embedded on a Texas Instruments microcontroller
 * or used solely and exclusively in conjunction with a Texas Instruments radio
 * frequency transceiver, which is integrated into your product. Other than for
 * the foregoing purpose, you may not use, reproduce, copy, prepare derivative
 * works of, modify, distribute, perform, display or sell this Software and/or
 * its documentation for any purpose.
 *
 * YOU FURTHER ACKNOWLEDGE AND AGREE THAT THE SOFTWARE AND DOCUMENTATION ARE
 * PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY, TITLE,
 * NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL
 * TEXAS INSTRUMENTS OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER CONTRACT,
 * NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR OTHER
 * LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
 * INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE
 * OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT
 * OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
 * (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *
 * Should you have any questions regarding your right to use this Software,
 * contact Texas Instruments Incorporated at www.TI.com.
 *
 */

#include "tof_security.h"
   
#include "rtls/rtls_ctrl.h"

#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <ti/sysbios/hal/Hwi.h>
#include <ti/sysbios/knl/Swi.h>

#include <ti/drivers/crypto/CryptoCC26XX.h>

#include <TRNGCC26XX.h>

/*********************************************************************
 * INTERNAL FUNCTIONS
 */
 
static void     TOFSecurity_incrementByOne(uint8_t *v, uint16_t n);
static void     TOFSecurity_ctrDrbgUpdate(tofSecHandle_t *tofSecHandle, uint8_t *providedData);
static void     TOFSecurity_ctrDrbgInstantiate(tofSecHandle_t *tofSecHandle);
static uint16_t TOFSecurity_ctrDrbgGenerateBytes(tofSecHandle_t *tofSecHandle, uint8_t *dst, uint16_t length);
static int      TOFSecurity_AES128Encrypt(tofSecHandle_t *tofSecHandle, uint8_t *ciphertext);

/*********************************************************************
 * PUBLIC FUNCTIONS
 */

/*******************************************************************************
 * @fn          TOFSecurity_open API
 *
 * @brief       This function is used to open the tof security module
 *              (to default or to presets defined in tofSecCfgPrms) and
 *              reset the AES engine.
 *
 *              Note: This routine leaves CM3 Crypto interrupts disabled.
 *
 *              Note: This function allocates the key without specifying one.
 *                    The key is loaded as needed without releases the key
 *                    store.
 *
 * input parameters
 *
 * @param       tofSecCfgPrms  - A pointer to the user-defined tof security preset values.
 *
 * @param       tofSecHandle  - A pointer to an empty tof security handle.
 *
 * output parameters
 *
 * @param       tofSecHandle  - A pointer to the tof security state.
 *
 * @return      uint8_t - NULL if failed, other if succeeded
 */
int TOFSecurity_open(tofSecHandle_t *tofSecHandle, tofSecCfgPrms_t *tofSecCfgPrms)
{
  uint16_t sizeOfBuffToAlloc;

  // Sanity check
  if (tofSecHandle == NULL || tofSecCfgPrms == NULL)
  {
    return TOF_SEC_INVALID_PRMS_FAIL_E;
  }

  //  Verify params valid
  if((0 == tofSecCfgPrms->syncWordSize) || (0 == tofSecCfgPrms->totalNumOfSyncWords))
  {
    return TOF_SEC_INVALID_PRMS_FAIL_E;
  }
  
  // Copy configuration parameters to handle
  memcpy(&(tofSecHandle->tofSecCfgParams),tofSecCfgPrms, sizeof(tofSecCfgPrms_t));

  // Init buffer/s sizes
  if(TOF_MODE_DBL_BUF == tofSecHandle->tofSecCfgParams.bUseDoubleBuffer)
  {
    tofSecHandle->numOfSyncWordsPerBuffer = TOF_SEC_DBL_BUFF_SIZE;
  }
  else
  {
    tofSecHandle->numOfSyncWordsPerBuffer = tofSecCfgPrms->totalNumOfSyncWords;
  }

	// Calculate size of buffer/s
  sizeOfBuffToAlloc = sizeof(uint8_t) * tofSecCfgPrms->syncWordSize * tofSecHandle->numOfSyncWordsPerBuffer;

  // Allocate first buffer
  RTLSUTIL_MALLOC(tofSecHandle->pSyncWordBuffer1, sizeOfBuffToAlloc);
  if(NULL == (tofSecHandle->pSyncWordBuffer1))
  {
    return TOF_SEC_ALLOC_FAIL_E;
  }

  // Allocate 2nd buffer?
  if(TOF_MODE_DBL_BUF == tofSecCfgPrms->bUseDoubleBuffer)
  {  
    RTLSUTIL_MALLOC(tofSecHandle->pSyncWordBuffer2, sizeOfBuffToAlloc);
    if(NULL == (tofSecHandle->pSyncWordBuffer2))
    {
      RTLSUTIL_FREE(tofSecHandle->pSyncWordBuffer1);
      return TOF_SEC_ALLOC_FAIL_E;
    }
  }

  // Init AES128 hardware
  CryptoCC26XX_init();
  CryptoCC26XX_Params_init(&(tofSecHandle->encParams));

  tofSecHandle->encHandle = CryptoCC26XX_open(0, FALSE, &(tofSecHandle->encParams));
  if(NULL == tofSecHandle->encHandle)
  {
    RTLSUTIL_FREE(tofSecHandle->pSyncWordBuffer1);

    if(TOF_MODE_DBL_BUF == tofSecCfgPrms->bUseDoubleBuffer)
    {
      RTLSUTIL_FREE(tofSecHandle->pSyncWordBuffer2);
    }

    return TOF_SEC_AES_FAIL_E;
  }

  // allocate a key store index
  // Note: A key does not have to be specified at this time, and can be loaded as needed.
  tofSecHandle->encKey = CryptoCC26XX_allocateKey(tofSecHandle->encHandle, CRYPTOCC26XX_KEY_ANY, NULL);
  ASSERT(CRYPTOCC26XX_STATUS_ERROR != (int)tofSecHandle->encKey);

  return TOF_SEC_SUCCESS_E;
}

/*******************************************************************************
 * @fn          TOFSecurity_close API
 *
 * @brief       This function is used to close ToF Security and free buffers
 *
 * input parameters
 *
 * @param       tofSecHandle  - A pointer to an empty tof security handle.
 *
 * output parameters
 *
 * @return      uint8_t - NULL if failed, other if succeeded
 */
int TOFSecurity_close(tofSecHandle_t *tofSecHandle)
{
  // Free first buffer
  RTLSUTIL_FREE(tofSecHandle->pSyncWordBuffer1);

  // If double buffer is enabled, free the other one as well
  if (tofSecHandle->tofSecCfgParams.bUseDoubleBuffer == TOF_MODE_DBL_BUF)
  {
    RTLSUTIL_FREE(tofSecHandle->pSyncWordBuffer2);
  }

  CryptoCC26XX_releaseKey(tofSecHandle->encHandle, &tofSecHandle->encKey);

  return TOF_SEC_SUCCESS_E;
}

/*******************************************************************************
 * @fn          TOFSecurity_reseed API
 *
 * @brief       This function is used to reseed the ctr drbg
 *
 * input parameters
 *
 * @param       tofSecHandle  - A pointer to the tof security state.
 *
 * @param       newSeed  - A pointer to the 128-bit new seed with which to reinstantiate.
 *
 * output parameters
 *
 * @param       tofSecHandle  - A pointer to the tof security state.
 *
 * @return      int - NULL if failed, other if succeeded
 */
int TOFSecurity_setSeed(tofSecHandle_t *tofSecHandle, uint8_t *newSeed)
{
  if((NULL == tofSecHandle) || (NULL == newSeed))
  {
    return TOF_SEC_INVALID_PRMS_FAIL_E;
  }
  
  memcpy(tofSecHandle->seed, newSeed, CTR_DRBG_SEEDLEN);
  
  tofSecHandle->ctrDrbgState.instantiatedFlag = 0;

  return TOF_SEC_SUCCESS_E;
}


/*********************************************************************
* @fn      TOFSecurity_genSeed
*
* @brief   use TRNG HW module to generate a true 128-bit random seed
*
* input parameters
*
* @param   tofSecHandle  - A pointer to the tof security state.
*
* output parameters
*
* @param   tofSecHandle  - A pointer to the tof security state.
*
* @return  NULL if failed, other if succeeded
*/
int TOFSecurity_genSeed(tofSecHandle_t *tofSecHandle)
{
  uint32_t  tmp;
  uint16_t  i;

  for (i = 0; i < CTR_DRBG_SEEDLEN; i = i+4)
  {
    if(0 == (tmp = TRNGCC26XX_getNumber(NULL, NULL, NULL)))
    {
      return TOF_SEC_GEN_FAIL_E;
    }
    memcpy(&(tofSecHandle->seed[i]), &tmp, 4);
  } 

  tofSecHandle->ctrDrbgState.instantiatedFlag = 0;

  return TOF_SEC_SUCCESS_E;
}

/*********************************************************************
* @fn       TOFSecurity_genSyncWords
*
* @brief    Will fill each Sync Word Buffer with <numOfSyncWordsPerBuffer> Sync Words
*
* input parameters
*
* @param   bufferToFill  - An enum describing which buffers to fill
*
* @param   tofSecHandle  - A pointer to the tof security state.
*
* output parameters
*
* @param   tofSecHandle  - A pointer to the tof security state.
*
* @return  NULL if failed, other if succeeded
*/
int TOFSecurity_genSyncWords(tofSecHandle_t *tofSecHandle, uint8_t bufferToFill)
{
  if((TOF_SEC_FILL_BOTH_BUFFS == bufferToFill) || (TOF_SEC_FILL_1ST_BUFF == bufferToFill))
  {
    if(NULL != tofSecHandle->pSyncWordBuffer1)
    {
      TOFSecurity_ctrDrbgGenerateBytes(tofSecHandle, 
                                       tofSecHandle->pSyncWordBuffer1, 
                                       tofSecHandle->tofSecCfgParams.syncWordSize * tofSecHandle->numOfSyncWordsPerBuffer);
    }
    else
    {
      return TOF_SEC_GEN_FAIL_E;
    }
  }
  
  if((TOF_SEC_FILL_BOTH_BUFFS == bufferToFill) || (TOF_SEC_FILL_2ND_BUFF == bufferToFill))
  {
    if(tofSecHandle->tofSecCfgParams.bUseDoubleBuffer)
    {
      if(NULL != tofSecHandle->pSyncWordBuffer2)
      {
        TOFSecurity_ctrDrbgGenerateBytes(tofSecHandle, 
                                         tofSecHandle->pSyncWordBuffer2, 
                                         tofSecHandle->tofSecCfgParams.syncWordSize * tofSecHandle->numOfSyncWordsPerBuffer);
      }
      else
      {
        return TOF_SEC_GEN_FAIL_E;
      }
    }
  }
  return TOF_SEC_SUCCESS_E;
}


/*********************************************************************
 * CTR_DRBG FUNCTIONS
 */

/*******************************************************************************
 * @fn          TOFSecurity_incrementByOne API
 *
 * @brief       Helper function to increment elements of v by one with carry 
 *              while ensuring a constant number of cycles (i.e. regardless of 
 *              the number of carries in the operation).
 *
 * input parameters
 * 
 * @param       n - number of cycles.
 *
 * output parameters
 *
 * @param       v - A pointer to the value to be incremented.
 *
 * @return      None.
 */
static void TOFSecurity_incrementByOne(uint8_t *v, uint16_t n)
{
  int16_t i;
  uint16_t c;
  uint16_t x;

  // Increment elements of v by one with carry.
  for (i = n-1, c = 1; i >= 0; i--) 
  {
    x = (uint16_t)v[i] + c;

    v[i] = (uint8_t)(x & 0xff);
        
    c = x >> 8;
  }
}

/*******************************************************************************
 * @fn          TOFSecurity_ctrDrbgUpdate API
 *
 * @brief       Updates the CTR-DRBG internal state using AES-128 block algorithm based on              
 *              section 10.2.1.2 of NIST SP 800-90Ar1
 *
 *              This function updates the internal states v and key for the CTR-DRBG.
 *              If not being used for instantiation, provided_data should be zero.
 *
 * input parameters
 * 
 * @param       tofSecHandle  - A pointer to the tof security state.
 *
 * @param       providedData  - A pointer to the initial data input to the ctr drbg module.
 *
 * output parameters
 *
 * @param       tofSecHandle  - A pointer to the tof security state.
 *
 * @return      None.
 */
static void TOFSecurity_ctrDrbgUpdate(tofSecHandle_t *tofSecHandle, uint8_t *providedData)
{
  uint16_t i;
  uint8_t  temp[CTR_DRBG_SEEDLEN];
    
  // Zero initialize temp array
  memset(temp, 0, CTR_DRBG_SEEDLEN);
    
  // Build new Key and V with AES-128 Encryption
  for (i = 0; i < CTR_DRBG_SEEDLEN/CTR_DRBG_BLOCKLEN; i++) 
  {
    // Increment V by one
    TOFSecurity_incrementByOne(tofSecHandle->ctrDrbgState.v, CTR_DRBG_BLOCKLEN);

    // Encrypt V and store in temp encryptedData
    TOFSecurity_AES128Encrypt(tofSecHandle, &temp[i*CTR_DRBG_BLOCKLEN]);     
  }
  
  // If providedData is not NULL, XOR encryptedData with the entropy input(providedData)
  if (NULL != providedData)
  {
    for (i = 0; i < CTR_DRBG_SEEDLEN; i++) 
    {
      temp[i] ^= providedData[i];
    }
  }
  
  // Break temp apart into Key and V
  for (i = 0; i < CTR_DRBG_BLOCKLEN; i++)
  {
    tofSecHandle->ctrDrbgState.key[i] = temp[i];
    tofSecHandle->ctrDrbgState.v[i] = temp[i + CTR_DRBG_BLOCKLEN];
  }
}


/*******************************************************************************
 * @fn          TOFSecurity_ctrDrbgInstantiate API
 *
 * @brief       Instantiates the CTR-DRBG according to Section 9.1 and 10.2.1.3.1 of NIST SP
 *              800-90Ar1.
 *
 *              This function generates an initial working state for the CTR-DRBG and only
 *              needs to be called once per CTR-DRBG's lifetime.
 *
 * input parameters
 * 
 * @param       tofSecHandle  - A pointer to the tof security state.
 *
 * output parameters
 *
 * @param       tofSecHandle  - A pointer to the tof security state.
 *
 * @return      None.
 */
static void TOFSecurity_ctrDrbgInstantiate(tofSecHandle_t *tofSecHandle)
{
  // Initialize the working state to 0
  memset(tofSecHandle->ctrDrbgState.key, 0, CTR_DRBG_BLOCKLEN);
  memset(tofSecHandle->ctrDrbgState.v, 0, CTR_DRBG_BLOCKLEN);

  // Update the working state with the provided entropy(seed)
  TOFSecurity_ctrDrbgUpdate(tofSecHandle, tofSecHandle->seed);

  // Signal that the working state has been instantiated
  tofSecHandle->ctrDrbgState.instantiatedFlag = CTR_DRBG_INST_FLAG;
  
}


/*******************************************************************************
 * @fn          TOFSecurity_ctrDrbgGenerateBytes API
 *
 * @brief       Generates the requested number of random bytes using the AES-128 
 *              block cipher algorithm according to Section 9.3.1 and 10.2.1.5.1 
 *              of NIST SP 800-90Ar1.
 *
 *              This function generates the requested number of random bytes using the
 *              CTR-DRBG methodology.
 *
 *              Note: It is recommended (but NOT mandatory) to generate bytes in  
 *              multiples of 16 as it is the number of bytes generated by the ctr drbg 
 *              in this implementation.
 *
 * input parameters
 * 
 * @param       tofSecHandle  - A pointer to the tof security state.
 *
 * @param       length - The number of bytes to be generated by the ctr drbg module
 *
 * output parameters
 *
 * @param       dst - A pointer to the output buffer in which to insert the output data
 *
 * @return      The number of bytes generated or NULL in case of bad arguments.
 *
 */
static uint16_t TOFSecurity_ctrDrbgGenerateBytes(tofSecHandle_t *tofSecHandle,  uint8_t *dst, uint16_t length)
{
  uint16_t  i, j;
  uint8_t   temp[CTR_DRBG_SEEDLEN];
  uint16_t  numOfEncLoops;

	// Verify valid parameters
  if((NULL == dst) || (0 == length)) 
  {
    return TOF_SEC_INVALID_PRMS_FAIL_E;
  }

  // Instantiate working state if needed
  if(tofSecHandle->ctrDrbgState.instantiatedFlag != CTR_DRBG_INST_FLAG) 
  {
    TOFSecurity_ctrDrbgInstantiate(tofSecHandle);
  }

  numOfEncLoops = (length / CTR_DRBG_BLOCKLEN);

  // Check if an extra encryption iteration is needed 
  if ((length % CTR_DRBG_BLOCKLEN ) > 0)
  {
    numOfEncLoops++;
  }

  // Generate minimum number of bytes needed to fill <length> sync words
  for (i = 0; i < numOfEncLoops; i++)
  {
    // Clear buffer
    memset(temp, 0, CTR_DRBG_SEEDLEN);

    // Get 16 bytes and update the working state
    TOFSecurity_ctrDrbgUpdate(tofSecHandle, temp);

    for (j = 0; ((((i * CTR_DRBG_BLOCKLEN) + j) < length) && (j < CTR_DRBG_BLOCKLEN)); j++)
    {
      dst[((i * CTR_DRBG_BLOCKLEN) + j)] = tofSecHandle->ctrDrbgState.v[j];
    }
  }
  
  return TOF_SEC_SUCCESS_E;
}


/*********************************************************************
 * LOCAL FUNCTIONS
 */

/*******************************************************************************
 * @fn          TOFSecurity_AES128Encrypt API
 *
 * @brief       For internal driver:
 *              This function takes a key, plaintext, and generates ciphertext
 *              by AES128 encryption. 
 *
 *              For the external driver:
 *              This function calls the driver transact to perform AES ECB
 *              encryption based on the provided key and plaintext.
 *
 * input parameters
 *
 * @param       tofSecHandle  - A pointer to the tof security state.
 *
 * output parameters
 *
 * @param       ciphertext - The 128 bit cipher text after encryption.
 *
 * @return      None.
 */
static int TOFSecurity_AES128Encrypt(tofSecHandle_t *tofSecHandle, uint8_t *ciphertext)
{
  int res;

  // Verify valid parameters
  if(NULL == ciphertext)
  {
    return TOF_SEC_INVALID_PRMS_FAIL_E;
  }
  
  // load the key at the key store index
  res = CryptoCC26XX_loadKey(tofSecHandle->encHandle, tofSecHandle->encKey, (uint32_t *)tofSecHandle->ctrDrbgState.key);

  CryptoCC26XX_AESECB_Transaction encTrans;

  encTrans.opType   = CRYPTOCC26XX_OP_AES_ECB;
  encTrans.keyIndex = tofSecHandle->encKey;
  encTrans.msgIn    = tofSecHandle->ctrDrbgState.v;
  encTrans.msgOut   = ciphertext;


  // Note: Use polling for the AES transaction to . 
  //For non-polling use: CryptoCC26XX_transact(tofSecHandle->encHandle,(CryptoCC26XX_Transaction *)&encTrans)
  
  res = CryptoCC26XX_transactPolling(tofSecHandle->encHandle, (CryptoCC26XX_Transaction *)&encTrans);

  return res;
}

