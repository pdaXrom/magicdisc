#!/bin/bash

#srec_cat OAD_BIM_offchip_cc2640r2lp/FlashOnly_unsecure/OAD_BIM_offchip_cc2640r2lp.hex -intel \
#    OAD_App_Offchip_cc2640r2lp/FlashROM_unsecure/OAD_App_Offchip_cc2640r2lp_FlashROM_unsecure.hex -intel \
#    OAD_Stack_Offchip_cc2640r2lp/FlashROM_unsecure/OAD_Stack_Offchip_cc2640r2lp_FlashROM_unsecure.hex -intel \
#    -o factory.hex -intel

srec_cat OAD_BIM_offchip_cc2640r2lp/FlashOnly_unsecure/OAD_BIM_offchip_cc2640r2lp.hex -intel \
    OAD_App_Offchip_cc2640r2lp/FlashROM_unsecure/OAD_App_Offchip_cc2640r2lp_FlashROM_unsecure_oad.bin -binary -offset 0x0 \
    OAD_Stack_Offchip_cc2640r2lp/FlashROM_unsecure/OAD_Stack_Offchip_cc2640r2lp_FlashROM_unsecure_oad.bin -binary -offset 0x13000 \
    -o factory.hex -intel
