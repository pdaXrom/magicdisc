#ifndef RGB_MATRIX_H
#define RGB_MATRIX_H

#ifdef __cplusplus
extern "C"
{
#endif
/*******************************************************************************
 *                                          Includes
 */
#include <inc/hw_prcm.h>
#include <driverlib/prcm.h>

#include "bsp.h"

  /*******************************************************************************
 * MACROS
 */

#define STATUS_UPDATE   0
#define STATUS_ERROR    8
#define STATUS_FACTORY  16
#define STATUS_OKAY     24
#define STATUS_HEART    32
#define STATUS_RECOVERY 40

extern void delay(uint32_t delayMs);

extern void RGB_powerUp(void);

extern void RGB_powerDown(void);

extern void RGB_showStatus(int status);

#endif /* RGB_MATRIX_H */
