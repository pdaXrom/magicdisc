/*
 * Copyright (c) 2015-2019, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== uartecho.c ========
 */
#include <stdint.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>

#include <math.h>

#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Semaphore.h>
#include <xdc/runtime/Error.h>

/* Driver Header files */
#include <ti/drivers/GPIO.h>
#include <ti/drivers/PWM.h>
//#include <ti/drivers/I2C.h>
#include <ti/drivers/UART.h>

#include <ti/drivers/ADC.h>
#include <ti/drivers/adc/ADCCC26XX.h>

/* Example/Board Header files */
#include "Board.h"
#include "uart_misc.h"

#include "matrix.h"
#include "rgb_matrix.h"

#include "adxl345.h"

#include "scif.h"

#define BV(n)               (1 << (n))

// Semaphore used to wait for Sensor Controller task ALERT event
static Semaphore_Struct semScTaskAlert;

int maxLightTime = 100; // 5 s.

void scCtrlReadyCallback(void)
{
} // scCtrlReadyCallback

void scTaskAlertCallback(void)
{
    // Wake up the OS task
    Semaphore_post(Semaphore_handle(&semScTaskAlert));
} // scTaskAlertCallback

/*
 *  ======== mainThread ========
 */
void mainTask(UArg a0, UArg a1)
{
    // Create the semaphore used to wait for Sensor Controller ALERT events
    Semaphore_Params semParams;
    Semaphore_Params_init(&semParams);
    semParams.mode = Semaphore_Mode_BINARY;
    Semaphore_construct(&semScTaskAlert, 0, &semParams);

    /* Call driver init functions */
    GPIO_init();
    UART_init();

    RGB_init();

    RGB_textWrite("Project Z  ");
/*
 * UART driver
 */
    UART_Handle uart;
    UART_Params uartParams;

    /* Create a UART with data processing off. */
    UART_Params_init(&uartParams);
    uartParams.writeDataMode = UART_DATA_BINARY;
    uartParams.readDataMode = UART_DATA_BINARY;
    uartParams.readReturnMode = UART_RETURN_FULL;
    uartParams.readEcho = UART_ECHO_OFF;
    uartParams.baudRate = 115200;

    uart = UART_open(Board_UART0, &uartParams);

    if (uart == NULL) {
        /* UART_open() failed */
        while (1);
    }

    UART_Setup(uart);

    // Initialize the Sensor Controller
    scifOsalInit();
    scifOsalRegisterCtrlReadyCallback(scCtrlReadyCallback);
    scifOsalRegisterTaskAlertCallback(scTaskAlertCallback);
    scifInit(&scifDriverSetup);

    // Set the Sensor Controller task tick interval to 1 second
    scifStartRtcTicksNow(0x00010000 / 10);

    static const uint8_t ADXL345_InitData[SCIF_I2C_ACCELEROMETER_ADXL_INIT_DATASIZE] =
    {
     ADXL345_REG_POWER_CTL, 0x00,
     ADXL345_REG_POWER_CTL, 0x08,
     ADXL345_REG_DATA_FORMAT, ADXL345_RANGE_2G,
     ADXL345_REG_THRESH_TAP, 0x50,
     ADXL345_REG_LATENT, 0x05,
     ADXL345_REG_WINDOW, 0xFF,
     ADXL345_REG_DUR, 0x10,
     ADXL345_REG_TAP_AXES, 0x07,
     ADXL345_REG_INT_ENABLE, 0x60,
    };

    int n;

    for (n = 0; n < SCIF_I2C_ACCELEROMETER_ADXL_INIT_DATASIZE; n++) {
        scifTaskData.i2cAccelerometer.cfg.INITDATA[n] = ADXL345_InitData[n];
    }

    scifTaskData.i2cAccelerometer.input.tapOnly = 1;

    scifStartTasksNbl(BV(SCIF_I2C_ACCELEROMETER_TASK_ID) | BV(SCIF_ANALOG_BATTERY_SENSOR_TASK_ID));

#if 1

    while (RGB_isScrollingEnabled()) {
        Task_sleep(50000);
    }
    RGB_setLight(0);

/*
 * Main logic
 */
    int lightTime = maxLightTime;
    int lightLevelMax = 25; // 25%
    int lightLevelInc = 1;
    int lightLevel = 0;

    int32_t adcOffset = AUXADCGetAdjustmentOffset(AUXADC_REF_FIXED);
    int32_t adcGainError = AUXADCGetAdjustmentGain(AUXADC_REF_FIXED);

    /* Loop forever echoing */
    while (1) {
        // Wait for an ALERT callback
        Semaphore_pend(Semaphore_handle(&semScTaskAlert), BIOS_WAIT_FOREVER);

        // Clear the ALERT interrupt source
        scifClearAlertIntSource();

////        char bufx[128];
////        snprintf(bufx, sizeof(bufx), "X=%d Y=%d Z=%d\n\r", (scifTaskData.i2cAccelerometer.output.x * 90) / 260, scifTaskData.i2cAccelerometer.output.y, scifTaskData.i2cAccelerometer.output.z);
////        snprintf(bufx, sizeof(bufx), "X=%d Y=%d Z=%d tap=%d\n\r", scifTaskData.i2cAccelerometer.output.x, scifTaskData.i2cAccelerometer.output.y, scifTaskData.i2cAccelerometer.output.z, scifTaskData.i2cAccelerometer.output.isDoubleTap);
////        UART_SendString(bufx);


        if (scifTaskData.i2cAccelerometer.output.isDoubleTap) {
             UART_SendString("Double Tap detected!\n\r");
             lightTime = maxLightTime;
        } else {
             if (lightLevel <= 0) {
//                     UART_SendString("Sleeping\n\r");

                 // Acknowledge the alert event
                 scifAckAlertEvents();
                 continue;
             }
        }

        if (lightTime == 0) {
             if (lightLevel > 0) {
                 lightLevel -= lightLevelInc;
                 RGB_setLight(lightLevel);
                 if (lightLevel == 0) {
                     scifTaskData.i2cAccelerometer.input.tapOnly = 1;
                 }
             }
         } else {
            if (lightLevel < lightLevelMax) {
                lightLevel += lightLevelInc;
                RGB_setLight(lightLevel);
                if (lightLevel == 1) {
                    scifTaskData.i2cAccelerometer.input.tapOnly = 0;
                }
            }
            lightTime--;
        }


        uint32_t adcValue = scifTaskData.analogBatterySensor.output.adcValue;
        uint32_t adcCorrectedValue = AUXADCAdjustValueForGainAndOffset((int32_t) adcValue, adcGainError, adcOffset);
        uint32_t adcValueMicroVolt = AUXADCValueToMicrovolts(AUXADC_FIXED_REF_VOLTAGE_NORMAL, adcCorrectedValue);

        char buf[128];
        snprintf(buf, sizeof(buf), "X=%d Y=%d Z=%d BAT=%d\n\r", (scifTaskData.i2cAccelerometer.output.x * 90) / 260,
                 scifTaskData.i2cAccelerometer.output.y, scifTaskData.i2cAccelerometer.output.z,
                 adcValueMicroVolt);
        UART_SendString(buf);

        if (!RGB_isScrollingEnabled()) {
            RGB_rotateFb(0, 0, degreesToRadians((scifTaskData.i2cAccelerometer.output.x * 90) / 260));
        }

#if 0
////////////////
            static float X_out, Y_out, Z_out;  // Outputs
            static float roll,pitch,rollF,pitchF=0;

            X_out = (float)x / 256;
            Y_out = (float)y / 256;
            Z_out = (float)z / 256;

//            snprintf(buf, sizeof(buf), "%f/%f/%f\n\r", X_out, Y_out, Z_out);
//            UART_SendString(uart, buf);

            roll = atan(Y_out / sqrt(pow(X_out, 2) + pow(Z_out, 2))) * 180 / M_PI;
            pitch = atan(-1 * X_out / sqrt(pow(Y_out, 2) + pow(Z_out, 2))) * 180 / M_PI;

            rollF = 0.94 * rollF + 0.06 * roll;
            pitchF = 0.94 * pitchF + 0.06 * pitch;

            snprintf(buf, sizeof(buf), "%f/%f\n\r", rollF, pitchF);
            UART_SendString(uart, buf);
////////////////
#endif
////        UART_read(uart, &input, 1);
////        UART_write(uart, &input, 1);

        // Acknowledge the alert event
        scifAckAlertEvents();

    }
#endif
}
