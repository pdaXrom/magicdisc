/*
 * uart_misc.h
 *
 *  Created on: 12 ����. 2019 �.
 *      Author: sash
 */

#ifndef UART_MISC_H_
#define UART_MISC_H_

void UART_Setup(void *uart);

void UART_SendString(char *string);

#endif /* UART_MISC_H_ */
