/*
 * rgb_matrix.c
 *
 *  Created on: 17 ����. 2019 �.
 *      Author: sash
 */

#include <string.h>

#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/knl/Task.h>
#include <xdc/runtime/Error.h>

/* Driver Header files */
#include <ti/drivers/GPIO.h>
#include <ti/drivers/PWM.h>

#include "Board.h"

#include "matrix.h"
#include "font.h"

#include "rgb_matrix.h"

#include "uart_misc.h"

static PWM_Handle pwm1 = NULL;
static const uint16_t   pwmPeriod = 3000;

static Clock_Handle clkHandle;
static char clkStopped;

/*
 * Framebuffer memory
 */

#define FB_R_MASK   (3 << 4)
#define FB_G_MASK   (3 << 2)
#define FB_B_MASK   (3 << 0)

static uint8_t fbMemory[8][8] = {
     { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 },
     { 0x00, 0x20, 0x20, 0x00, 0x00, 0x20, 0x20, 0x00 },
     { 0x20, 0x04, 0x04, 0x20, 0x20, 0x01, 0x01, 0x20 },
     { 0x20, 0x04, 0x04, 0x04, 0x01, 0x01, 0x01, 0x20 },
     { 0x20, 0x04, 0x04, 0x00, 0x00, 0x01, 0x01, 0x20 },
     { 0x00, 0x20, 0x04, 0x04, 0x01, 0x01, 0x20, 0x00 },
     { 0x00, 0x00, 0x20, 0x04, 0x01, 0x20, 0x00, 0x00 },
     { 0x00, 0x00, 0x00, 0x20, 0x20, 0x00, 0x00, 0x00 },
};

static uint8_t fbMemoryPriv[8][16] = {
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 },
    { 0x00, 0x20, 0x20, 0x00, 0x00, 0x20, 0x20, 0x00 },
    { 0x20, 0x04, 0x04, 0x20, 0x20, 0x01, 0x01, 0x20 },
    { 0x20, 0x04, 0x04, 0x04, 0x01, 0x01, 0x01, 0x20 },
    { 0x20, 0x04, 0x04, 0x00, 0x00, 0x01, 0x01, 0x20 },
    { 0x00, 0x20, 0x04, 0x04, 0x01, 0x01, 0x20, 0x00 },
    { 0x00, 0x00, 0x20, 0x04, 0x01, 0x20, 0x00, 0x00 },
    { 0x00, 0x00, 0x00, 0x20, 0x20, 0x00, 0x00, 0x00 },
};

static uint16_t scanLine = 0;
static uint16_t scrollingCounter = 0;
static uint16_t scrollingDelay = 20;
static uint16_t enableScrolling = 0;

static char textLine[64];
static uint8_t textColor = 0x20;
static uint16_t textPosCounter = 0;

void RGB_putChar(char ch, uint8_t color, int pos)
{
    int i, j;
    for (i = 0; i < 5; i++) {
        uint8_t byte = font5x7[ch][i];
        for (j = 0; j < 7; j++) {
            fbMemoryPriv[j][pos + i] = (byte & 1) ? color : 0;
            byte >>= 1;
        }
    }
}

static Void displayClock(UArg arg1)
{
    int i;
    unsigned int line; // = 0x00ffff00; // 10010010 01001001 00100100
    unsigned char b, rb, gb, bb;

    for (i = 0; i < 8; i++) {
        b = fbMemoryPriv[i][scanLine];
        rb = (rb << 1) | (b & FB_R_MASK ? 0 : 1);
        gb = (gb << 1) | (b & FB_G_MASK ? 0 : 1);
        bb = (bb << 1) | (b & FB_B_MASK ? 0 : 1);
    }

    line = (rb << 24) | (gb << 8) | (bb << 16);

    line |= (1 << scanLine);

    scanLine = (scanLine + 1) & 0x07;

    for (i = 0; i < 32; i++) {
        GPIO_write(Board_RGB_DIN, line & 1);
        line >>= 1;
        GPIO_write(Board_RGB_SCLK, 1);
//        Task_sleep(1);
        GPIO_write(Board_RGB_SCLK, 0);
    }
    GPIO_write(Board_RGB_LATCH, 1);
//    Task_sleep(1);
    GPIO_write(Board_RGB_LATCH, 0);

    if (!scanLine && enableScrolling) {
        scrollingCounter++;
        if (scrollingCounter == scrollingDelay) {
            scrollingCounter = 0;
            int i, j;
            for (j = 0; j < 8; j++) {
                for (i = 0; i < 15; i++) {
                    fbMemoryPriv[j][i] = fbMemoryPriv[j][i+1];
                }
            }

            textPosCounter++;
            if (textPosCounter > 5) {
                textPosCounter = 0;
                if (*textLine) {
                    RGB_putChar(*textLine, textColor, 9);
                    strcpy(textLine, textLine + 1);
                } else {
                    enableScrolling = 0;
                }
            }
        }
    }
}

int RGB_init()
{
    /*
     * PWM for rgb matrix light
     */
        /* Period and duty in microseconds */
        uint16_t   pwmDuty = 0;

        PWM_Params pwmParams;

        /* Call driver init functions. */
        PWM_init();

        PWM_Params_init(&pwmParams);
        pwmParams.dutyUnits = PWM_DUTY_US;
        pwmParams.dutyValue = pwmPeriod - pwmDuty;
        pwmParams.periodUnits = PWM_PERIOD_US;
        pwmParams.periodValue = pwmPeriod;
        pwm1 = PWM_open(Board_PWM0, &pwmParams);
        if (pwm1 == NULL) {
            /* Board_PWM0 did not open */
            return 1;
        }

        PWM_start(pwm1);

    /*
     * RGB matrix
     */
        GPIO_write(Board_RGB_NRESET, 0);
        Task_sleep(100);
        GPIO_write(Board_RGB_NRESET, 1);

        Clock_Params clockParams;

        Clock_Params_init(&clockParams);
        clockParams.period = 100;        // 0 is the default, but added for completeness
        clockParams.startFlag = TRUE;
        clockParams.arg = 'A';
        clkHandle = Clock_create(displayClock, 1000, &clockParams, Error_IGNORE);

        clkStopped = 0;

        memset(textLine, 0, sizeof(textLine));

        RGB_setLight(25);

        //strcpy(textLine, "Project Z ");

        //enableScrolling = 1;

        return 0;
}

void RGB_textWrite(char *text)
{
    strncpy(textLine, text, sizeof(textLine));
    enableScrolling = 1;
}

void RGB_setLight(int level)
{
    if (level == 0) {
        Clock_stop(clkHandle);
        clkStopped = 1;
        UART_SendString("Display stopped\n\r");
    } else if (clkStopped) {
        Clock_start(clkHandle);
        clkStopped = 0;
        UART_SendString("Display started\n\r");
    }
    PWM_setDuty(pwm1, pwmPeriod - (pwmPeriod * level) / 100);
}

int RGB_isScrollingEnabled()
{
    return enableScrolling;
}

void RGB_rotateFb(float angleX, float angleY, float angleZ)
{
    memset(fbMemoryPriv, 0, sizeof(fbMemoryPriv));
    matrixRotate(fbMemory, fbMemoryPriv, angleX, angleY, angleZ);

}
