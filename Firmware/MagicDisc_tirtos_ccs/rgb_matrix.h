/*
 * rgb_matrix.h
 *
 *  Created on: 17 ����. 2019 �.
 *      Author: sash
 */

#ifndef RGB_MATRIX_H_
#define RGB_MATRIX_H_

int RGB_init();

void RGB_setLight(int level);

int RGB_isScrollingEnabled();

void RGB_putChar(char ch, uint8_t color, int pos);

void RGB_textWrite(char *text);

void RGB_rotateFb(float angleX, float angleY, float angleZ);

#endif /* RGB_MATRIX_H_ */
