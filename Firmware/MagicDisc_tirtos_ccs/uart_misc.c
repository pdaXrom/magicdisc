#include <stdint.h>
#include <stddef.h>
#include <string.h>

#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/knl/Task.h>
#include <xdc/runtime/Error.h>

/* Driver Header files */
//#include <ti/drivers/GPIO.h>
//#include <ti/drivers/PWM.h>
//#include <ti/drivers/I2C.h>
#include <ti/drivers/UART.h>

static UART_Handle _uart;

void UART_Setup(void *uart)
{
    _uart = uart;
}

void UART_SendString(char *string)
{
    UART_write(_uart, string, strlen(string));
}
