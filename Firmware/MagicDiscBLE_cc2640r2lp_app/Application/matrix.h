#ifndef __MATRIX_H__
#define __MATRIX_H__

typedef struct {
    float	x;
    float	y;
    float	z;
} Dot3D;

#define degreesToRadians(angleDegrees) ((angleDegrees) * M_PI / 180.0)
#define radiansToDegrees(angleRadians) ((angleRadians) * 180.0 / M_PI)

Dot3D *rotateX(Dot3D *dot, float angle);
Dot3D *rotateY(Dot3D *dot, float angle);
Dot3D *rotateZ(Dot3D *dot, float angle);

void matrixRotate(uint8_t src[8][8], uint8_t dst[8][16], float angleX, float angleY,
                  float angleZ);
#endif
