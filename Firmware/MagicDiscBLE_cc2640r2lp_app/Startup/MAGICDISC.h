/*
 * Copyright (c) 2016-2019, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
/** ============================================================================
 *  @file       CC2640R2_LAUNCHXL.h
 *
 *  @brief      CC2640R2 LaunchPad Board Specific header file.
 *
 *  This file is responsible for setting up the board specific items for the
 *  CC2640R2_LAUNCHXL board.
 *
 *  This board file is made for the 7x7 mm QFN package, to convert this board
 *  file to use for other smaller device packages(5x5 mm and 4x4 mm QFN), the
 *  board will need to be modified since all the IO pins are not available for
 *  smaller packages. Note that the 2.7 x 2.7 mm WCSP package should use a
 *  separate board file also included within the SDK.
 *
 *  Refer to the datasheet for all the package options and IO descriptions:
 *  http://www.ti.com/lit/ds/symlink/cc2640r2f.pdf
 *
 *  For example, to change to the 4x4 package, remove all defines for all IOs
 *  not available (IOID_10 and higher) since the 4x4 package
 *  has only 10 DIO pins as listed in the datasheet. Remove the modules/pins
 *  not used including ADC, Display, SPI1, LED, and PIN due to limited pins.
 *  ============================================================================
 */
#ifndef __CC2640R2_MAGICDISC_BOARD_H__
#define __CC2640R2_MAGICDISC_BOARD_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Includes */
#include <ti/drivers/PIN.h>
#include <ti/devices/cc26x0r2/driverlib/ioc.h>

/* Externs */
extern const PIN_Config BoardGpioInitTable[];

/* Defines */
#ifndef CC2640R2_MAGICDISC
  #define CC2640R2_MAGICDISC
#endif /* CC2640R2_MAGICDISC */

/*
 *  ============================================================================
 *  RF Front End and Bias configuration symbols for TI reference designs and
 *  kits. This symbol sets the RF Front End configuration in ble_user_config.h
 *  and selects the appropriate PA table in ble_user_config.c.
 *  Other configurations can be used by editing these files.
 *
 *  Define only one symbol:
 *  CC2650EM_7ID    - Differential RF and internal biasing
                      (default for CC2640R2 LaunchPad)
 *  CC2650EM_5XD    – Differential RF and external biasing
 *  CC2650EM_4XS    – Single-ended RF on RF-P and external biasing
 *  CC2640R2DK_CXS  - WCSP: Single-ended RF on RF-N and external biasing
 *                    (Note that the WCSP is only tested and characterized for
 *                     single ended configuration, and it has a WCSP-specific
 *                     PA table)
 *
 *  Note: CC2650EM_xxx reference designs apply to all CC26xx devices.
 *  ==========================================================================
 */
#define CC2650EM_7ID


/* RGB MATRIX */
#define MAGICDISC_RGB_DIN               IOID_12
#define MAGICDISC_RGB_SCLK              IOID_13
#define MAGICDISC_RGB_LATCH             IOID_14
#define MAGICDISC_RGB_NOE               IOID_15
#define MAGICDISC_RGB_NRESET            IOID_18

#define MAGICDISC_PWMPIN0               MAGICDISC_RGB_NOE

//#define MAGICDISC_I2C0_SCL0             IOID_4
//#define MAGICDISC_I2C0_SDA0             IOID_5

/* SPI Board */
#define MAGICDISC_SPI0_MISO             IOID_10          /* RF1.20 */
#define MAGICDISC_SPI0_MOSI             IOID_8           /* RF1.18 */
#define MAGICDISC_SPI0_CLK              IOID_9           /* RF1.16 */
#define MAGICDISC_SPI0_CSN              PIN_UNASSIGNED

#define MAGICDISC_SPI_FLASH_CS          IOID_11
/* SPI */
#define MAGICDISC_FLASH_CS_ON           0
#define MAGICDISC_FLASH_CS_OFF          1

/* UART Board */
#define MAGICDISC_UART_RX               IOID_2          /* RXD */
#define MAGICDISC_UART_TX               IOID_3          /* TXD */

/*!
 *  @brief  Initialize the general board specific settings
 *
 *  This function initializes the general board specific settings.
 */
void MAGICDISC_initGeneral(void);


/*!
 *  @def    CC2640R2_LAUNCHXL_GPIOName
 *  @brief  Enum of GPIO names
 */
typedef enum MAGICDISC_GPIOName {
    MAGICDISC_GPIO_RGB_DIN,
    MAGICDISC_GPIO_RGB_SCLK,
    MAGICDISC_GPIO_RGB_LATCH,
    MAGICDISC_GPIO_RGB_NOE,
    MAGICDISC_GPIO_RGB_NRESET,
    MAGICDISC_GPIO_SPI_FLASH_CS,
    MAGICDISC_GPIOCOUNT
} MAGICDISC_GPIOName;

/*!
 *  @def    CC2640R2_LAUNCHXL_GPTimerName
 *  @brief  Enum of GPTimer parts
 */
typedef enum MAGICDISC_GPTimerName {
    MAGICDISC_GPTIMER0A = 0,
    MAGICDISC_GPTIMERPARTSCOUNT
} MAGICDISC_GPTimerName;

/*!
 *  @def    CC2640R2_LAUNCHXL_GPTimers
 *  @brief  Enum of GPTimers
 */
typedef enum MAGICDISC_GPTimers {
    MAGICDISC_GPTIMER0 = 0,
    MAGICDISC_GPTIMERCOUNT
} MAGICDISC_GPTimers;

/*!
 *  @def    CC2640R2_MAGICDISC_NVSName
 *  @brief  Enum of NVS names
 */
typedef enum CC2640R2_MAGICDISC_NVSName {
#ifndef Board_EXCLUDE_NVS_INTERNAL_FLASH
    CC2640R2_MAGICDISC_NVSCC26XX0 = 0,
#endif
#ifndef Board_EXCLUDE_NVS_EXTERNAL_FLASH
    CC2640R2_MAGICDISC_NVSSPI25X0,
#endif

    CC2640R2_MAGICDISC_NVSCOUNT
} CC2640R2_MAGICDISC_NVSName;

/*!
 *  @def    CC2640R2_LAUNCHXL_PWM
 *  @brief  Enum of PWM outputs
 */
typedef enum MAGICDISC_PWMName {
    MAGICDISC_PWM0 = 0,
    MAGICDISC_PWMCOUNT
} MAGICDISC_PWMName;

/*!
 *  @def    CC2640R2_LAUNCHXL_I2CName
 *  @brief  Enum of I2C names
 */
//typedef enum MAGICDISC_I2CName {
//    MAGICDISC_I2C0 = 0,
//
//    MAGICDISC_I2CCOUNT
//} CC2640R2_LAUNCHXL_I2CName;

/*!
 *  @def    CC2640R2_LAUNCHXL_SPIName
 *  @brief  Enum of SPI names
 */
typedef enum MAGICDISC_SPIName {
    MAGICDISC_SPI0 = 0,
    MAGICDISC_SPICOUNT
} MAGICDISC_SPIName;

/*!
 *  @def    CC2640R2_LAUNCHXL_UARTName
 *  @brief  Enum of UARTs
 */
typedef enum MAGICDISC_UARTName {
    MAGICDISC_UART0 = 0,
    MAGICDISC_UARTCOUNT
} MAGICDISC_UARTName;

/*!
 *  @def    CC2640R2_MAGICDISC_UDMAName
 *  @brief  Enum of DMA buffers
 */
typedef enum CC2640R2_MAGICDISC_UDMAName {
    CC2640R2_MAGICDISC_UDMA0 = 0,

    CC2640R2_MAGICDISC_UDMACOUNT
} CC2640R2_MAGICDISC_UDMAName;

/*!
 *  @def    CC2640R2_MAGICDISC_CryptoName
 *  @brief  Enum of Crypto names
 */
typedef enum MAGICDISC_CryptoName {
    MAGICDISC_CRYPTO0 = 0,

    MAGICDISC_CRYPTOCOUNT
} MAGICDISC_CryptoName;

/*!
 *  @def    CC2640R2_MAGICDISC_WatchdogName
 *  @brief  Enum of Watchdogs
 */
typedef enum MAGICDISC_WatchdogName {
    MAGICDISC_WATCHDOG0 = 0,
    MAGICDISC_WATCHDOGCOUNT
} MAGICDISC_WatchdogName;

/*!
 *  @def    CC2650_MAGICDISC_TRNGName
 *  @brief  Enum of TRNG names on the board
 */
typedef enum MAGICDISC_TRNGName {
    MAGICDISC_TRNG0 = 0,
    MAGICDISC_TRNGCOUNT
} MAGICDISC_TRNGName;

#ifdef __cplusplus
}
#endif

#endif /* __CC2640R2_MAGICDISC_BOARD_H__ */
